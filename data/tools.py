# --------------------------------------------------------------------
# A Planet's Revenge
# Copyright (C) 2008 Michael Harmer
# --------------------------------------------------------------------
# This file is part of A Planet's Revenge (APR).
# --------------------------------------------------------------------
# APR is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# --------------------------------------------------------------------
# APR is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# --------------------------------------------------------------------
# You should have received a copy of the GNU General Public License
# along with APR.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------
# For more information contact me at: mick_harmer91@hotmail.com
# --------------------------------------------------------------------

import curses

def RemoveCursor(screen):
     screen.addstr(74,24,"",curses.color_pair(1))     
    
def ClearLine(screen,line):
    """clears a given line on the screen"""        
    for x in range(1,79):
        screen.addstr(line,x," ",curses.color_pair(1))

def InsertLine(screen,line,char,colour):
    """adds a separater across a line"""
    for x in range(1,79):
        screen.addstr(line,x,char,curses.color_pair(colour))

def Scroll(screen,data,posy,posx,dis_lim,cur_dis,c_pair):
    """Allows a list of text to be 'scrolled' through"""
    count = 0
    try:
        for i in range(cur_dis,cur_dis+dis_lim):
            screen.addstr(posy+count,posx,data[i],curses.color_pair(c_pair)|curses.A_BOLD)
            count += 1
    except:
        pass

def Import(file_loc):
    """Import text from a file into a list line by line."""
    dump = []
    for line in file(file_loc):
        dump.append(line)
    return dump

