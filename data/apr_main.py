# --------------------------------------------------------------------
# A Planet's Revenge
# Copyright (C) 2008 Michael Harmer
# --------------------------------------------------------------------
# This file is part of A Planet's Revenge (APR).
# --------------------------------------------------------------------
# APR is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# --------------------------------------------------------------------
# APR is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# --------------------------------------------------------------------
# You should have received a copy of the GNU General Public License
# along with APR.  If not, see <http://www.gnu.org/licenses/>.
# --------------------------------------------------------------------
# For more information contact me at: mick_harmer91@hotmail.com
# --------------------------------------------------------------------

import sys, traceback, random, time, tools

try:
    import curses
except:
    print "Critical Error: Curses cannot be loaded. Please contact Michael!"
    time.sleep(5)
    
version = "1.0a"

def main(screen):
    global stdscr
    stdscr = screen    
    curses.init_pair(1,curses.COLOR_RED,curses.COLOR_BLACK)
    curses.init_pair(2,curses.COLOR_GREEN,curses.COLOR_BLACK)
    curses.init_pair(3,curses.COLOR_BLUE,curses.COLOR_BLACK)
    curses.init_pair(4,curses.COLOR_YELLOW,curses.COLOR_BLACK)
    curses.init_pair(5,curses.COLOR_CYAN,curses.COLOR_BLACK)
    curses.init_pair(6,curses.COLOR_MAGENTA,curses.COLOR_BLACK)
    curses.init_pair(7,curses.COLOR_WHITE,curses.COLOR_BLACK)
    curses.init_pair(8,curses.COLOR_WHITE,curses.COLOR_WHITE)
    curses.init_pair(9,curses.COLOR_BLACK,curses.COLOR_BLACK)
    curses.init_pair(10,curses.COLOR_BLUE,curses.COLOR_BLUE)
    
    #Runs the game menu
    MENU()

#############
# Game Menu # #NOTE: Will place getting a player choice into one method later, for now it can appear in every menu.
#############

class MENU:
    def __init__(self):
        self.menu_1()

    def menu_1(self):        
        line = 11
        line_index = line
        
        stdscr.addstr(8,24,"A Planet's Revenge",curses.color_pair(heading) |curses.A_BOLD)
        stdscr.addstr(9,35,"by Michael Harmer",curses.color_pair(text_main))

        CHOICE = None

        while CHOICE != 13: #ENTER
            stdscr.addstr(line,24,"> new game",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(line + 1,24,"> high scores",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(line + 2,24,"> exit",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(line_index,24,">",curses.color_pair(text_main))
            tools.RemoveCursor(stdscr)
            stdscr.refresh()

            CHOICE = stdscr.getch()
            
            if CHOICE == 259: #UP
                if line_index == line: #Cannot move beyond the top option
                    pass
                else:
                    line_index -= 1
                
            elif CHOICE == 258: #DOWN
                if line_index == line + 2: #Or below the bottom option
                    pass
                else:
                    line_index += 1

        if line_index == line:
            self.menu_2()
        elif line_index == line + 1:
            print "Yeah, maybe in a future release."
        elif line_index == line + 2:
            GAME.QUIT(1)               
         
    def menu_2(self):
        for i in range(11,14):
            tools.ClearLine(stdscr,i)

        line = 13
        line_index = line
            
        stdscr.addstr(11,24,"Select difficulty level:",curses.color_pair(text_main) |curses.A_BOLD)

        CHOICE = None

        while CHOICE != 13 and CHOICE != 8 and CHOICE != 27: #ENTER or BACKSPACE or ESCAPE
            stdscr.addstr(13,24,"> noob",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(14,24,"> easy",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(15,24,"> normal",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(16,24,"> hard",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(17,24,"> expert",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(line_index,24,">",curses.color_pair(text_main))
            tools.RemoveCursor(stdscr)
            stdscr.refresh()

            CHOICE = stdscr.getch()
            
            if CHOICE == 259: #UP
                if line_index == line: #Cannot move beyond the top option
                    pass
                else:
                    line_index -= 1
                
            elif CHOICE == 258: #DOWN
                if line_index == line + 4: #Or below the bottom option
                    pass
                else:
                    line_index += 1

        if CHOICE == 8 or CHOICE == 27: #If player wants to go back a screen
            self.menu_1()

        else:
            difficulties = { line:1, line + 1:2, line + 2:3, line + 3:4, line + 4:5 }
            GAME.difficulty = difficulties[line_index]            
            self.menu_3()

    def menu_3(self):
        for i in range(11,18):
            tools.ClearLine(stdscr,i)

        line = 13
        line_index = line
            
        stdscr.addstr(11,24,"<insert story here>",curses.color_pair(text_main) |curses.A_BOLD)    
        stdscr.addstr(13,24,"> Press any key to continue..",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(line_index,24,">",curses.color_pair(text_main))
        tools.RemoveCursor(stdscr)
        
        stdscr.refresh()        
        CHOICE = stdscr.getch()
        
        if CHOICE == 8 or CHOICE == 27: #Player goes back...
            self.menu_2()
        else:
            self.menu_4()

    def menu_4(self):
        for i in range(11,24):
            tools.ClearLine(stdscr,i)

        line = 13
        line_index = line

        objectives = {line:"Colonise 10 planets in phase 10 space.",
                      line + 1:"Bribe your enemy with 5 million credits.",
                      line + 2:"Destroy all enemies in the solar system."}

        stdscr.addstr(11,24,"Select a primary objective:",curses.color_pair(text_main) |curses.A_BOLD)    

        CHOICE = None

        while CHOICE != 13 and CHOICE != 8 and CHOICE != 27: #ENTER or BACKSPACE or ESCAPE
            stdscr.addstr(13,24,"> Colonise 10 planets in phase 10 space.",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(14,24,"> Bribe your enemy with 5 million credits.",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(15,24,"> Destroy all enemies in the solar system.",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(line_index,24,">",curses.color_pair(text_main))
            tools.RemoveCursor(stdscr)
            stdscr.refresh()

            CHOICE = stdscr.getch()
            
            if CHOICE == 259: #UP
                if line_index == line: #Cannot move beyond the top option
                    pass
                else:
                    line_index -= 1
                
            elif CHOICE == 258: #DOWN
                if line_index == line + 2: #Or below the bottom option
                    pass
                else:
                    line_index += 1

        if CHOICE == 8 or CHOICE == 27:
            self.MENU_3()
        else:
            GAME.objective = objectives[line_index]
            dot = "Loading: "
            for i in range(1,69):
                dot += "."
                stdscr.clear()
                stdscr.addstr(24,1,dot,curses.color_pair(heading) |curses.A_BOLD)
                stdscr.refresh()
                time.sleep(0.0275)
                
            #Displays the main screen initially
            GAME.MAIN()
            #Main game loop for getting inputs and exits
            while 1:
                INPUT = stdscr.getch()
                GAME.DISPLAY(INPUT)                        
        

###############################
# Empire Setup and Management #
###############################

class GAME:
    def __init__(self):
        self.set_stats()
        
    def set_stats(self):
        """sets base stats and variables for the player"""
        #The lists for resources are: [quantity, min, max, avg, current, supdem, name]
        #Loads resource values from a file, else just loads defaults
        RESOURCES = []
        try:
            for line in file("resources.ini", "r+"):
                if list(line)[0] != "#":
                    RESOURCES.append(line[:-1])
        except:
            print "Error: resources.ini missing, loading default values."
            RESOURCES = [100,10,50,1,10,5,5,2500,0,50,150,100,100,2500,0,10,40,25,25,2500,0,20,80,50,50,2500,0,5,25,15,15,2500]

        self.credits = int(RESOURCES[0])
        self.turn = 0
        self.action_points = int(RESOURCES[1])
        #For each resource have 
        self.food = [int(RESOURCES[2]),int(RESOURCES[3]),int(RESOURCES[4]),int(RESOURCES[5]),int(RESOURCES[6]),int(RESOURCES[7]),"food"]        
        self.luxis = [int(RESOURCES[8]),int(RESOURCES[9]),int(RESOURCES[10]),int(RESOURCES[11]),int(RESOURCES[12]),int(RESOURCES[13]),"luxis"]        
        self.steel = [int(RESOURCES[14]),int(RESOURCES[15]),int(RESOURCES[16]),int(RESOURCES[17]),int(RESOURCES[18]),int(RESOURCES[19]),"steel"]        
        self.titanium = [int(RESOURCES[20]),int(RESOURCES[21]),int(RESOURCES[22]),int(RESOURCES[23]),int(RESOURCES[24]),int(RESOURCES[25]),"titanium"]        
        self.eptum = [int(RESOURCES[26]),int(RESOURCES[27]),int(RESOURCES[28]),int(RESOURCES[29]),int(RESOURCES[30]),int(RESOURCES[31]),"eptum"]        
        self.GAME_RESOURCES = [self.food,self.luxis,self.steel,self.titanium,self.eptum]
        self.ARTEFACTS = {"Small Research Capsule":0,\
                          "Medium Research Capsule":0,\
                          "Massive Research Capsule":0,\
                          "Miracle Seeds":0,\
                          "Alchemist's Orb":0,\
                          "Metallic Shard":0,\
                          "Eptum Emitter":0,\
                          "Trade Beacon":0,\
                          "Timewarp Device":0,\
                          "Ancient Temple":0}
        self.MAX_PLANETS = 1        
        self.MSGLOG = []
        self.has_planet = False
        self.PP = 0
        self.ships_pending = 0
        self.current_tech = None
        self.difficulty = None
        self.objective = None
        self.status = None #Can be None, "win" or "lose"        
        self.donated = 0        
        self.total_ships_destroyed = 0
        self.total_satellites_destroyed = 0
        self.total_planets_destroyed = 0
        self.alien_level = 1
        
    def DISPLAY(self,command = "MAIN"):
        """takes an arguement from a main input and chooses what to display"""
        self.command = command
        self.MAIN()        

        if self.command == 27: #ESC
            self.QUIT(1)               
        elif self.command == 32: #SPACE - testing
            self.CHEAT(100)
        elif self.command == 8: #BACKSPACE - save game
            pass
        elif self.command == 13: #ENTER - end turn
            self.TURN()
        
        elif self.command == ord("b") and self.objective == "Bribe your enemy with 5 million credits.": #"b" - bribe enemy
            self.BRIBE()
        
        elif self.command == 265: #F1 - help
            self.HELP()
        elif self.command == ord("p"):
            self.PLANETS()           
        elif self.command == ord("e"):
            self.EXPLORE()            
        elif self.command == ord("r"):
            self.RESEARCH()         
        elif self.command == ord("s"):
            self.SATELLITES()        
        elif self.command == ord("m"):
            self.MARKET()
        elif self.command == ord("a"):
            self.ARTEFACT_VIEW()
        elif self.command == ord("l"):
            self.LOG_VIEW()
        elif self.command == ord("w"):
            self.WORKFORCE()
        stdscr.refresh()

    def QUIT(self,time_to_wait):
        stdscr.clear()
        stdscr.addstr(11,10,"Uninstalling Windows. Do not reset your PC. Have a nice day!",curses.color_pair(heading) |curses.A_BOLD)            
        stdscr.refresh()
        time.sleep(time_to_wait)            
        quit()    

    def HELP(self):
        """access the tutorial, controls, overview and credits"""
        stdscr.clear()        
        stdscr.addstr(1,1,"HELP:",curses.color_pair(heading) |curses.A_BOLD)        
        tools.InsertLine(stdscr,2,"-",border)
        stdscr.addstr(3,1,"New players will want to look at the tutorial:",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(3,48,"Press 1.",curses.color_pair(secondary) |curses.A_BOLD)
        stdscr.addstr(5,1,"For a complete list of controls:",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(5,34,"Press 2.",curses.color_pair(secondary) |curses.A_BOLD)
        stdscr.addstr(7,1,"To view the credits:",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(7,22,"Press 3.",curses.color_pair(secondary) |curses.A_BOLD)        
        stdscr.addstr(19,1,"BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)       
        self.BOTH_BG()
        ACTION = stdscr.getch()
        for i in range(1,20):
            tools.ClearLine(stdscr,i)
        if ACTION == ord("1"):            
            self.TUTORIAL()
        elif ACTION == ord("2"):            
            self.CONTROLS()
        elif ACTION == ord("3"):
            self.CREDITS()
        elif ACTION == 8:
            self.DISPLAY()
        else:            
            self.HELP()

    def TUTORIAL(self):
        """goes through a series of screens explaining APR"""
        stdscr.addstr(1,1,"APR TUTORIAL:",curses.color_pair(heading) |curses.A_BOLD)
        tools.InsertLine(stdscr,2,"-",border)
        tools.InsertLine(stdscr,18,"-",border)      
        stdscr.addstr(19,1,"PAGEUP/PAGEDOWN: Scroll up/down, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)        
        self.BOTH_BG()        
        tutorial = tools.Import("tutorial")
        
        cur_dis = 0
        dis_lim = 15
        while 1:
            for i in range(3,18):
                tools.ClearLine(stdscr,i)
            tools.Scroll(stdscr,tutorial,3,1,dis_lim,cur_dis,text_main)
            stdscr.refresh() 
            INPUT = stdscr.getch()
            if INPUT == 339:
                cur_dis -= 1
                if cur_dis < 0:
                    cur_dis = 0
            elif INPUT == 338:
                cur_dis += 1
                if cur_dis > len(tutorial) - dis_lim:
                    cur_dis = len(tutorial) - dis_lim
            elif INPUT == 8:
                break              
        self.HELP()

    def CONTROLS(self):
        """provides the main game controls"""        
        stdscr.addstr(1,1,"GAME CONTROLS:",curses.color_pair(heading) |curses.A_BOLD)
        tools.InsertLine(stdscr,2,"-",border)
        stdscr.addstr(3,1,"Note: Use the letter in brackets to access that area from the main prompt.",curses.color_pair(text_main) |curses.A_BOLD)                    
        stdscr.addstr(4,1,"( )lanets     - Access and control your planets.",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(4,2,"p",curses.color_pair(heading) |curses.A_BOLD)
        stdscr.addstr(5,1,"( )xplore     - Explore new phases for planets.",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(5,2,"e",curses.color_pair(heading) |curses.A_BOLD)        
        stdscr.addstr(6,1,"( )esearch    - Research new technologies and upgrades.",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(6,2,"r",curses.color_pair(heading) |curses.A_BOLD)
        stdscr.addstr(7,1,"( )hipyard    - Build a number of different types of spaceships.",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(7,2,"s",curses.color_pair(heading) |curses.A_BOLD)
        stdscr.addstr(8,1,"( )leet       - Manage and command your fleet of ships.",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(8,2,"f",curses.color_pair(heading) |curses.A_BOLD)
        stdscr.addstr(9,1,"( )arket      - Buy and sell a number of resources for credit.",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(9,2,"m",curses.color_pair(heading) |curses.A_BOLD)
        stdscr.addstr(10,1,"(a)rtefacts   - View your artefacts and details about them.",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(10,2,"a",curses.color_pair(heading) |curses.A_BOLD)    
        stdscr.addstr(11,1,"( )og         - View a complete list of all messages recieved.",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(11,2,"l",curses.color_pair(heading) |curses.A_BOLD)        
        stdscr.addstr(19,1,"BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)
        tools.InsertLine(stdscr,12,"-",border)
        self.BOTH_BG()
        RETURN = stdscr.getch()
        if RETURN == 8:
            self.HELP()
        else:
            self.CONTROLS()

    def CREDITS(self):
        """provides details on the making of the game"""
        stdscr.addstr(1,1,"CREDITS:",curses.color_pair(heading) |curses.A_BOLD)
        tools.InsertLine(stdscr,2,"-",border)
        stdscr.addstr(3,1,"Designed and programmed by Michael Harmer.",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(4,1,"Uses Python 2.5.1 with Curses.",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(5,1,"May 2008 >>> ?",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(6,1,"Game Version: %s" % version,curses.color_pair(text_main) |curses.A_BOLD)
        tools.InsertLine(stdscr,7,"-",border)
        stdscr.addstr(19,1,"BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)        
        self.BOTH_BG()
        RETURN = stdscr.getch()
        if RETURN == 8:
            self.HELP()
        else:
            self.CREDITS()

    def COLONISED_UPDATE(self):
        """Updates/creates the dictionary with the numbers of colonised planets per phase"""
        #Creates dictionary
        self.COLONISED_DICT = {}        
        for i in range(1,11):
            self.COLONISED_DICT[str(i)] = 0
        #Checks all planets
        for i in PLANET_DICT:
            if PLANET_DICT[i].colonised == True:                
                self.COLONISED_DICT[str(PLANET_DICT[i].phase)] += 1        
        #Sets max amounts of planet per phase according to "Planet Control" research
        planet_amounts = {0:1,1:2,2:2,3:3,4:3,5:4,6:5,7:6,8:7,9:8,10:10}
        self.MAX_PLANETS = planet_amounts[TECH_DICT["Planet Control"][0]]        

    def MAX_PHASE_UPDATE(self):
        """Updates the value for the maximum phase a player can explore"""
        self.max_phase = 0
        temp = []        
        #Checks for the highest phase in which the player has planets in
        #First simply runs through the planet counts and appends to a list as well as to the actual variable
        for i in self.COLONISED_DICT:
            if self.COLONISED_DICT[i] >= int(i):
                temp.append(int(i))               
                self.max_phase = int(i)                
        #Then checks to see if the variable has the maximum phase with planets in it as it's value, if not it sets it
        for x in temp:            
            if x > self.max_phase:
                self.max_phase = x                
        #This because players can explore one phase past the last phase they have planets in        
        self.max_phase = self.max_phase + 1
        #And the last possible phase is phase 10
        if self.max_phase > 10:
            self.max_phase = 10        

    def PLANETS(self):
        """manage and view planets"""
        self.COLONISED_UPDATE()
        stdscr.clear()        
        stdscr.addstr(1,1,"PLANETS:",curses.color_pair(heading) |curses.A_BOLD)
        tools.InsertLine(stdscr,2,"-",border)
        stdscr.addstr(3,1,"Planet Control:",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(4,1,"Phase 1: "+str(self.COLONISED_DICT["1"])+"/"+str(self.MAX_PLANETS),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(5,1,"Phase 2: "+str(self.COLONISED_DICT["2"])+"/"+str(self.MAX_PLANETS),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(6,1,"Phase 3: "+str(self.COLONISED_DICT["3"])+"/"+str(self.MAX_PLANETS),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(7,1,"Phase 4: "+str(self.COLONISED_DICT["4"])+"/"+str(self.MAX_PLANETS),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(8,1,"Phase 5: "+str(self.COLONISED_DICT["5"])+"/"+str(self.MAX_PLANETS),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(9,1,"Phase 6: "+str(self.COLONISED_DICT["6"])+"/"+str(self.MAX_PLANETS),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(10,1,"Phase 7: "+str(self.COLONISED_DICT["7"])+"/"+str(self.MAX_PLANETS),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(11,1,"Phase 8: "+str(self.COLONISED_DICT["8"])+"/"+str(self.MAX_PLANETS),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(12,1,"Phase 9: "+str(self.COLONISED_DICT["9"])+"/"+str(self.MAX_PLANETS),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(13,1,"Phase 10: "+str(self.COLONISED_DICT["10"])+"/"+str(self.MAX_PLANETS),curses.color_pair(text_main) |curses.A_BOLD)
        tools.InsertLine(stdscr,14,"-",border)
        for i in self.COLONISED_DICT:
            if self.COLONISED_DICT[i] > self.MAX_PLANETS:
                stdscr.addstr(15,1,"Warning: You have exceeded your planet control limit.",curses.color_pair(text_main) |curses.A_BOLD)
                stdscr.addstr(16,1,"         You may not manage planets over the limit.",curses.color_pair(text_main) |curses.A_BOLD)
                stdscr.addstr(17,1,"         To raise the limit, research Planet Control.",curses.color_pair(text_main) |curses.A_BOLD)
                tools.InsertLine(stdscr,18,"-",border)
        tools.ClearLine(stdscr,19)
        stdscr.addstr(19,1,"1-10: Select a phase to view, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)                
        self.BOTH_BG()
        choices = {49:1,50:2,51:3,52:4,53:5,54:6,55:7,56:8,57:9,48:10}
        ACTION = stdscr.getch()        
        if ACTION == 8:
            self.DISPLAY()
        elif ACTION in choices:            
            self.PLANET_PHASE(choices[ACTION])
        else:            
            self.PLANETS()
            
    def PLANET_PHASE(self,choice):
        """shows planets in a certain phase"""        
        for i in range(3,19):
                tools.ClearLine(stdscr,i)
        stdscr.addstr(1,1,"PLANETS ... PHASE "+str(choice)+" SPACE:",curses.color_pair(heading) |curses.A_BOLD)        
        stdscr.refresh()
        #Uses the following variables to cycle through all colonized planets and display them in a list with a letter next to the name
        alpha = ["a","b","c","d","e","f","g","h","i","j",None] # < Extra entry on end, too lazy to find the actual error but this works...       
        displaying = 0        
        linechange = 3
        marker_change = 0
        self.marker = alpha[marker_change]
        for i in PLANET_DICT:                                                          #Below is to only display planets under the planet limit
            if PLANET_DICT[i].phase == choice and PLANET_DICT[i].colonised == True and PLANET_DICT[i].destroy == False and displaying < self.MAX_PLANETS:
                PLANET_DICT[i].marker = self.marker                
                stdscr.addstr(linechange,1,"("+self.marker+") "+i,curses.color_pair(text_main) |curses.A_BOLD)
                linechange += 1
                marker_change += 1                
                self.marker = alpha[marker_change]
                displaying += 1
                stdscr.refresh()
        tools.ClearLine(stdscr,18)
        tools.ClearLine(stdscr,19)
        
        try: #Quick hack for if planets get destroyed
            stdscr.addstr(19,1,"a-"+alpha[displaying - 1]+": Access a planet, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)
        except:
            pass
        
        stdscr.refresh()                   
        ACTION = stdscr.getch()
        ACTION = chr(ACTION)
        if ACTION in alpha:
            for i in PLANET_DICT:                    
                if PLANET_DICT[i].marker == ACTION and PLANET_DICT[i].phase == choice:                    
                    planet_choice = PLANET_DICT[i]
                    #Gives the planet instance and the phase to the manager
                    self.PLANET_MANAGE(planet_choice,choice)                    
                    break
                
        elif ord(ACTION) == 8:
            self.PLANETS()
        else:
            self.PLANET_PHASE(choice)

    def PLANET_MANAGE(self,planet,choice):        
        """manage a particular planet"""        
        while 1:            
            for i in range(3,19):
                tools.ClearLine(stdscr,i)
            stdscr.addstr(1,1,"PLANETS ... PHASE "+str(choice)+" SPACE ... "+planet.name+":",curses.color_pair(heading) |curses.A_BOLD)
            
            #This turns "[mineral1,mineral2,mineral3]" into "mineral1, mineral2, mineral3"
            mineprint = ""
            count = 1
            for i in planet.PLANET_MINERALS:
                if count == 1:
                    mineprint += i
                else:
                    mineprint += ", "+i
                count += 1

            if mineprint == "":
                mineprint == "--- none ---"
                
            stdscr.addstr(3,1,"Type: "+planet.type_name+" ... Size: "+str(planet.size)+" ... Minerals: "+mineprint,curses.color_pair(text_main) |curses.A_BOLD)                      
            stdscr.addstr(4,1,"Population (Current/Max/Growth): "+str(planet.pop_current)+"/"+str(planet.pop_max)+"/+"+str(planet.pop_growth)+" per turn.",curses.color_pair(text_main) |curses.A_BOLD)
            #Gets the 3 values for food and formats them
            food_plus = planet.INFRA_CURRENT["ag"] * 2 + 3
            food_minus = planet.pop_current / 10
            food_balance = food_plus - food_minus
            if food_balance == 0:
                food_balance = "-"
            elif food_balance > 0:
                food_balance = "+"+str(food_balance)
            elif food_balance < 0:
                food_balance = str(food_balance)
            stdscr.addstr(5,1,"Food Status (Producing/Consuming/Balance): "+str(food_plus)+"/"+str(food_minus)+"/"+food_balance+" per turn.",curses.color_pair(text_main) |curses.A_BOLD)            
            stdscr.addstr(6,1,"Revenue (Taxes): +"+str((planet.INFRA_CURRENT["commerce"]/2 + 5) * (planet.pop_current / 10))+" credits per turn.",curses.color_pair(text_main) |curses.A_BOLD)            
            tools.InsertLine(stdscr,7,"-",border)
            stdscr.addstr(8,1,"Infrastructure Levels (Current/Max/Cost):",curses.color_pair(text_main) |curses.A_BOLD)
            costs = {planet.INFRA_CURRENT["lifesupport"]:0,planet.INFRA_CURRENT["mine"]:0,planet.INFRA_CURRENT["ag"]:0,planet.INFRA_CURRENT["science"]:0,planet.INFRA_CURRENT["commerce"]:0,planet.INFRA_CURRENT["industry"]:0}
            for i in costs:
                costs[i] = i ** 2 * 2 + 10                
            stdscr.addstr(9,1,"(1) Life Support: "+str(planet.INFRA_CURRENT["lifesupport"])+"/"+str(planet.INFRA_MAX["lifesupport"])+"/"+str(costs[planet.INFRA_CURRENT["lifesupport"]])+" credits.",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(10,1,"(2) Mining: "+str(planet.INFRA_CURRENT["mine"])+"/"+str(planet.INFRA_MAX["mine"])+"/"+str(costs[planet.INFRA_CURRENT["mine"]])+" credits.",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(11,1,"(3) Agriculture: "+str(planet.INFRA_CURRENT["ag"])+"/"+str(planet.INFRA_MAX["ag"])+"/"+str(costs[planet.INFRA_CURRENT["ag"]])+" credits.",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(12,1,"(4) Science: "+str(planet.INFRA_CURRENT["science"])+"/"+str(planet.INFRA_MAX["science"])+"/"+str(costs[planet.INFRA_CURRENT["science"]])+" credits.",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(13,1,"(5) Commerce: "+str(planet.INFRA_CURRENT["commerce"])+"/"+str(planet.INFRA_MAX["commerce"])+"/"+str(costs[planet.INFRA_CURRENT["commerce"]])+" credits.",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(14,1,"(6) Industry: "+str(planet.INFRA_CURRENT["industry"])+"/"+str(planet.INFRA_MAX["industry"])+"/"+str(costs[planet.INFRA_CURRENT["industry"]])+" credits.",curses.color_pair(text_main) |curses.A_BOLD)
            tools.InsertLine(stdscr,15,"-",border)
            stdscr.addstr(16,1,"Surface Exploration (Current/Total): "+str(planet.explore_amount[0])+"/"+str(planet.explore_amount[1]),curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(17,1,"Planet Health (Current/Total): "+str(planet.planet_health[0])+"/"+str(planet.planet_health[1]),curses.color_pair(text_main) |curses.A_BOLD)
            tools.InsertLine(stdscr,18,"-",border)
            tools.ClearLine(stdscr,19)
            stdscr.addstr(19,1,"1-6: Build infrastructure, e: Explore surface, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)
            tools.ClearLine(stdscr,22)
            self.BOTH_BG()
            stdscr.refresh()
            
            actions = {49:"lifesupport",\
                       50:"mine",\
                       51:"ag",\
                       52:"science",\
                       53:"commerce",\
                       54:"industry"}            
            
            ACTION = stdscr.getch()
            if ACTION in actions:
                #Requirements for building: 1 action point, enough credits and you must not have reached the max infra level
                if self.action_points >= 1:
                    if planet.INFRA_CURRENT[actions[ACTION]] < planet.INFRA_MAX[actions[ACTION]]:
                        nocred = 0 #Needs to be set initially
                        if self.credits >= costs[planet.INFRA_CURRENT[actions[ACTION]]]:
                            self.action_points -= 1
                            self.credits -= costs[planet.INFRA_CURRENT[actions[ACTION]]]
                            planet.INFRA_CURRENT[actions[ACTION]] += 1
                            self.LOG_ADD("Infrastructure built.",text_main)
                        else:                                                      
                            self.LOG_ADD("No credits available.",text_main)
                    else:                        
                        self.LOG_ADD("Maximum infrastructure level reached.",text_main)
                else:                    
                    self.LOG_ADD("No action points available.",text_main)
            elif ACTION == ord("e"):
                if self.action_points >= 1:
                    if planet.explore_amount[0] < planet.explore_amount[1]:
                        self.action_points -= 1
                        planet.explore_amount[0] += 1
                        self.EVENTS("explore surface",planet)
                    else:
                        self.LOG_ADD(planet.name+" is completely explored.",text_main)
                else:
                    self.LOG_ADD("No action points available.",text_main)
            elif ACTION == 8:
                for i in range(1,19):
                    tools.ClearLine(stdscr,i)
                break
            
        self.PLANET_PHASE(choice)        
            
    def EXPLORE(self):
        """explore new phases and planets"""
        self.COLONISED_UPDATE()
        self.MAX_PHASE_UPDATE()
        self.explore_chance = 5 + (self.max_phase) + (8 * TECH_DICT["Rapid Expansion"][0])
        stdscr.clear()
        stdscr.addstr(1,1,"EXPLORE:",curses.color_pair(heading) |curses.A_BOLD)
        tools.InsertLine(stdscr,2,"-",border)        
        stdscr.addstr(3,1,"The furthermost phase you may explore is phase "+str(self.max_phase)+".",curses.color_pair(text_main) |curses.A_BOLD)
        if self.has_planet == False:
            stdscr.addstr(4,1,"Your current chance of finding a planet is 100%.",curses.color_pair(text_main) |curses.A_BOLD)
        elif self.has_planet == True:            
            stdscr.addstr(4,1,"Your current chance of finding a planet is "+str(self.explore_chance)+"%.",curses.color_pair(text_main) |curses.A_BOLD)
        tools.InsertLine(stdscr,5,"-",border)
        tools.ClearLine(stdscr,19)
        stdscr.addstr(19,1,"1-"+str(self.max_phase)+": Select a phase to explore, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)        
        self.BOTH_BG()
        choices = {49:1,50:2,51:3,52:4,53:5,54:6,55:7,56:8,57:9,48:10}
        ACTION = stdscr.getch()
        if ACTION == 8:
            self.DISPLAY()
        elif ACTION in choices and self.action_points >= 1:
            self.action_points -= 1           
            self.EXPLORE_PHASE(choices[ACTION])
        else:
            self.EXPLORE()

    def EXPLORE_PHASE(self,choice):
        """explore for planets in a certain phase"""        
        if self.has_planet == False:
            self.explore_chance = 100 #First planet has 100% chance of being found                
        stdscr.addstr(6,1,"Exploring phase "+str(choice)+" space...",curses.color_pair(text_main) |curses.A_BOLD)
        tools.InsertLine(stdscr,7,"-",border)
        #Gets the amount of colonisable planets in the chosen phase
        planet_count = 0
        for random_planet in PLANET_DICT:            
            if PLANET_DICT[random_planet].phase == choice and PLANET_DICT[random_planet].colonised == False:               
                planet_count += 1           
        #Checks to see if there are any uncolonised planets in the chosen phase
        if planet_count >= 1:
            phase_full = False
        else:
            phase_full = True
            #Refund action points
            self.action_points += 1
        #Uses a random number between 1 and the number of colonisable planets to randomly select a planet to be colonised        
        if phase_full == False:
            current_count = 0
            seed = random.randint(1,planet_count)
            for random_planet in PLANET_DICT:
                if PLANET_DICT[random_planet].phase == choice and PLANET_DICT[random_planet].colonised == False:
                    current_count += 1
                    if current_count == seed:
                        break                
        #Checks to see if the player can explore the phase that is chosen
        if choice <= self.max_phase and self.action_points >= 1 and phase_full == False:            
            stdscr.addstr(6,1,"Your fleet of exploration ships report back to you:",curses.color_pair(text_main) |curses.A_BOLD)
            seed = random.randint(1,100)
            if seed <= self.explore_chance:                                         
                stdscr.addstr(6,53,"Planet discovered!",curses.color_pair(text_main) |curses.A_BOLD)
                stdscr.addstr(8,1,PLANET_DICT[random_planet].name+" appears similar to known "+PLANET_DICT[random_planet].type_name+" planets.",curses.color_pair(text_main) |curses.A_BOLD)
                size_desc = {1:"a meaningless",2:"a miniscule",3:"a tiny",4:"a small",5:"an average",6:"a decent",7:"a good",8:"a very good",9:"a great",10:"an amazing"}
                self.EVENTS("colonise")
                #Uses col_mod to adjust colonisation cost
                #print "The cost was :"+str(PLANET_DICT[random_planet].cost)
                PLANET_DICT[random_planet].cost += PLANET_DICT[random_planet].size * self.col_mod * 100
                #print "The col_mod is: "+str(self.col_mod)+" and the new cost is now: "+str(PLANET_DICT[random_planet].cost)
                #Swaps the positive/negative aspect around for adjusting the "expert analysis" description
                self.col_mod *= -1                
                try: #Tries to use the col_mod to make the desc better or worse
                    expert_message = size_desc[PLANET_DICT[random_planet].size + self.col_mod]
                except: #If key is < 1 or > 10, just use size
                    expert_message = size_desc[PLANET_DICT[random_planet].size]                
                stdscr.addstr(9,1,self.col_event,curses.color_pair(text_main) |curses.A_BOLD)
                stdscr.addstr(10,1,"Expert Analysis: This planet would be "+expert_message+" addition to our GAME.",curses.color_pair(text_main) |curses.A_BOLD)
                tools.InsertLine(stdscr,11,"-",border)
                if self.has_planet == True:
                    tools.ClearLine(stdscr,19)
                    stdscr.addstr(19,1,"ENTER: Colonise ("+str(PLANET_DICT[random_planet].cost)+" credits), BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)
                elif self.has_planet == False:
                    tools.ClearLine(stdscr,19)
                    stdscr.addstr(19,1,"ENTER: Colonise, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)                
                self.BOTH_BG()
                while 1:
                    COL_ACTION = stdscr.getch()
                    if COL_ACTION == 13 and self.has_planet == False: #Player is colonising 1st planet                    
                        self.has_planet = True                    
                        self.COLONISED_DICT[str(PLANET_DICT[random_planet].phase)] += 1                                                
                        PLANET_DICT[random_planet].colonised = True                    
                        self.LOG_ADD(PLANET_DICT[random_planet].name+" has been colonised.",text_main)
                        self.action_points -= 1
                        break
                    elif COL_ACTION == 13 and self.has_planet == True and self.credits >= PLANET_DICT[random_planet].cost: #Player is colonising further planets                 
                        self.COLONISED_DICT[str(PLANET_DICT[random_planet].phase)] += 1                                                
                        PLANET_DICT[random_planet].colonised = True
                        self.credits -= PLANET_DICT[random_planet].cost
                        self.LOG_ADD(PLANET_DICT[random_planet].name+" has been colonised.",text_main)
                        self.action_points -= 1
                        break
                    elif COL_ACTION == 8:                        
                        break                    
            else:
                stdscr.addstr(6,53,"Nothing to report!",curses.color_pair(text_main) |curses.A_BOLD)
                self.BOTH_BG()
                while 1:
                    RETURN = stdscr.getch()
                    if RETURN == 8:
                        break
            self.BOTH_BG()            
            self.EXPLORE()        
        else:            
            if phase_full == True:
                self.LOG_ADD("Phase "+str(choice)+" is completely explored.",text_main)                    
            elif self.action_points < 1:
                self.LOG_ADD("No action points available.",text_main)             
            elif choice > self.max_phase:
                self.LOG_ADD("You may not explore that far.",text_main)            
            self.EXPLORE()
            
    def RESEARCH(self):
        """research new technologies"""
        stdscr.clear()        
        stdscr.addstr(1,1,"RESEARCH:",curses.color_pair(heading) |curses.A_BOLD)
        tools.InsertLine(stdscr,2,"-",border)        
        stdscr.addstr(3,1,"TYPE:                               LEVEL:      CREDIT/TURNS FOR NEXT LEVEL:",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(4,1,"(1) Construction                    "+str(TECH_DICT["Construction"][0])+"/10        "+str(TECH_DICT["Construction"][1][TECH_DICT["Construction"][0]])+"/"+str(TECH_DICT["Construction"][2][TECH_DICT["Construction"][0]]),curses.color_pair(text_main) |curses.A_BOLD)        
        stdscr.addstr(5,1,"(2) Rapid Expansion                 "+str(TECH_DICT["Rapid Expansion"][0])+"/10        "+str(TECH_DICT["Rapid Expansion"][1][TECH_DICT["Rapid Expansion"][0]])+"/"+str(TECH_DICT["Rapid Expansion"][2][TECH_DICT["Rapid Expansion"][0]]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(6,1,"(3) Planetary Shielding             "+str(TECH_DICT["Planetary Shielding"][0])+"/10        "+str(TECH_DICT["Planetary Shielding"][1][TECH_DICT["Planetary Shielding"][0]])+"/"+str(TECH_DICT["Planetary Shielding"][2][TECH_DICT["Planetary Shielding"][0]]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(7,1,"(4) Planet Control                  "+str(TECH_DICT["Planet Control"][0])+"/10        "+str(TECH_DICT["Planet Control"][1][TECH_DICT["Planet Control"][0]])+"/"+str(TECH_DICT["Planet Control"][2][TECH_DICT["Planet Control"][0]]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(8,1,"(5) Space-Time Continuum            "+str(TECH_DICT["Space-Time Continuum"][0])+"/10        "+str(TECH_DICT["Space-Time Continuum"][1][TECH_DICT["Space-Time Continuum"][0]])+"/"+str(TECH_DICT["Space-Time Continuum"][2][TECH_DICT["Space-Time Continuum"][0]]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(9,1,"(6) Satellite Technology            "+str(TECH_DICT["Satellite Technology"][0])+"/10        "+str(TECH_DICT["Satellite Technology"][1][TECH_DICT["Satellite Technology"][0]])+"/"+str(TECH_DICT["Satellite Technology"][2][TECH_DICT["Satellite Technology"][0]]),curses.color_pair(text_main) |curses.A_BOLD)        
        tools.InsertLine(stdscr,10,"-",border)
        tools.ClearLine(stdscr,11)
        
        if self.current_tech != None:
            stdscr.addstr(11,1,"Your scientists are currently researching "+self.current_tech+".",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(12,1,"They will be finished in "+str(TECH_DICT[self.current_tech][2][TECH_DICT[self.current_tech][0]])+" turns.",curses.color_pair(text_main) |curses.A_BOLD)
            tools.InsertLine(stdscr,13,"-",border)
            stdscr.addstr(19,1,"BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)
            researching = True
            
        else:
            stdscr.addstr(11,1,"Your empire's scientists are currently idle.",curses.color_pair(text_main) |curses.A_BOLD)
            tools.InsertLine(stdscr,12,"-",border)
            stdscr.addstr(19,1,"1-8: Select research, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)
            researching = False

        self.BOTH_BG()
        techs = {49:"Construction",\
                 50:"Rapid Expansion",\
                 51:"Planetary Shielding",\
                 52:"Planet Control",\
                 53:"Space-Time Continuum",\
                 54:"Satellite Technology"}
        
        ACTION = stdscr.getch()

        if ACTION == 8:
            self.DISPLAY()
        
        elif ACTION in techs and researching == False:            
            if self.action_points >= 1:                
                if self.credits >= TECH_DICT[techs[ACTION]][1][TECH_DICT[techs[ACTION]][0]]:
                    #Decrease action points and credits, set current tech to the chosen tech
                    self.action_points -= 1
                    self.credits -= TECH_DICT[techs[ACTION]][1][TECH_DICT[techs[ACTION]][0]]
                    self.current_tech = techs[ACTION]
                    
            stdscr.refresh()
            self.RESEARCH()
            
        else:            
            self.RESEARCH()        
        
    def SATELLITES(self):        
        self.COLONISED_UPDATE()
        self.MAX_PHASE_UPDATE()
        stdscr.clear()        
        stdscr.addstr(1,1,"SATELLITES:",curses.color_pair(heading) |curses.A_BOLD)
        tools.InsertLine(stdscr,2,"-",border)
        stdscr.addstr(3,1,"Your satellites and enemy ships in each phase:",curses.color_pair(text_main) |curses.A_BOLD)
        line = 4
        for i in range(1,11):
            #A gap at the start to even up the rows
            if i != 10:
                gap = " "
            else:
                gap = ""

            #If phase has been explored, give info...
            if i <= self.max_phase:                
                stdscr.addstr(line,1,gap+str(i)+" | "+str(SATELLITES[i - 1][0])+" Laser | "+str(SATELLITES[i - 1][1])+" Shield | "+str(SATELLITES[i - 1][2])+" Trade | "+str(SHIPS[i - 1])+" Enemy Ships",curses.color_pair(text_main) |curses.A_BOLD)
            #Else give no info to the player
            else:
                stdscr.addstr(line,1,gap+str(i)+" | No intelligence.",curses.color_pair(text_main) |curses.A_BOLD)
                
            line += 1

        tools.InsertLine(stdscr,14,"-",border)         
        stdscr.addstr(15,1,"The current alien fleet level: "+str(self.alien_level),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(16,1,"Your current satellite level: "+str(TECH_DICT["Satellite Technology"][0]),curses.color_pair(text_main) |curses.A_BOLD)
        tools.InsertLine(stdscr,17,"-",border)
        stdscr.addstr(19,1,"b: Build satellites, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)
        self.BOTH_BG()            
                
        ACTION = stdscr.getch()
        
        if ACTION == 98:
            self.BUILD_SATELLITES()
        elif ACTION == 8:
            self.DISPLAY()        
        else:            
            self.SATELLITES()

    def BUILD_SATELLITES(self):
        self.COLONISED_UPDATE()
        self.MAX_PHASE_UPDATE()
        stdscr.clear()        
        stdscr.addstr(1,1,"SATELLITE PRODUCTION:",curses.color_pair(heading) |curses.A_BOLD)
        tools.InsertLine(stdscr,2,"-",border)
        
        stdscr.addstr(3,1,"Type:",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(4,1,"(1) Laser Satellite",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(5,1,"(2) Shield Satellite",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(6,1,"(3) Trade Satellite",curses.color_pair(text_main) |curses.A_BOLD)
        
        tools.InsertLine(stdscr,7,"-",border)                 
        stdscr.addstr(8,1,"Available Resources:",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(9,1,str(self.PP)+" Production Points, "+str(self.GAME_RESOURCES[2][0])+" Steel, "+str(self.GAME_RESOURCES[3][0])+" Titanium, "+str(self.GAME_RESOURCES[4][0])+" Eptum.",curses.color_pair(text_main) |curses.A_BOLD)

        tools.InsertLine(stdscr,10,"-",border)
        stdscr.addstr(19,1,"1-3: Select type, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)
        self.BOTH_BG()

        TYPES = {1:"Laser Satellite",2:"Shield Satellite",3:"Trade Satellite"}
        
        #Cost in Production Points, Steel, Titanium, Eptum...
        #Laser: 10 PP, 25 Titanium
        #Shield: 5 PP, 10 Eptum
        #Trade: 20 PP, 50 Steel
        COSTS = {1:[3,0,25,0],2:[2,0,0,10],3:[2,50,0,0]}
        
        #Need some way to go from costs to actual names for printing
        NAMES = {0:"PP",1:"Steel",2:"Titanium",3:"Eptum"}
        
        ACTION1 = stdscr.getch()
        try:
            ACTION1 = int(chr(ACTION1))
        except:
            pass

        if ACTION1 in TYPES:                
            stdscr.addstr(19,1,"1-"+str(self.max_phase)+": Select phase, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)
            stdscr.refresh()
            ACTION2 = stdscr.getch()
            try:
                ACTION2 = int(chr(ACTION2))
                if ACTION2 == 0:
                    ACTION2 = 10
            except:
                pass
            
            if ACTION2 in range(1,self.max_phase + 1):                
                cost_string = "A "+TYPES[ACTION1]+" in phase "+str(ACTION2)+" will cost: "

                #Run through the costs for the chosen satellite...
                for i in range(0,4):
                    #If it has a cost for a certain resource...
                    if COSTS[ACTION1][i] > 0:
                        #Add onto cost_string, eg "10 Eptum"                            
                        cost_string += str(COSTS[ACTION1][i]) + " " + NAMES[i]
                        #If i == 0, the resource if is the first one being checks and needs a comma afterwards
                        if i == 0:
                            cost_string += ", "
               
                stdscr.addstr(11,1,cost_string,curses.color_pair(text_main) |curses.A_BOLD)
                tools.InsertLine(stdscr,12,"-",border)
                stdscr.addstr(19,1,"ENTER: Build, SPACE: Cancel, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)
                stdscr.refresh()

                while 1:
                    ACTION3 = stdscr.getch()

                    if ACTION3 == 13:
                        #Check if player has enough resources
                        if self.PP >= COSTS[ACTION1][0]:
                            if self.GAME_RESOURCES[2][0] >= COSTS[ACTION1][1]:
                                if self.GAME_RESOURCES[3][0] >= COSTS[ACTION1][2]:
                                    if self.GAME_RESOURCES[4][0] >= COSTS[ACTION1][3]:
                                        self.PP -= COSTS[ACTION1][0]
                                        for i in range(1,4): #just an easy way of deleting resources...
                                            self.GAME_RESOURCES[i + 1][0] -= COSTS[ACTION1][i]

                                            #Increase SATELLITES[phase][type] by one, simulating a satellite being built into that phase
                                            SATELLITES[ACTION2 - 1][ACTION1 - 1] += 1
                                            
                        break
                    
                    elif ACTION3 == 32:
                        break

                self.BUILD_SATELLITES()

        elif ACTION1 == 8:
            self.SATELLITES()        
        else:            
            self.BUILD_SATELLITES()
                

    def WORKFORCE(self):
        #Cost will increase as player progresses further into the game (higher "turn" value)
        cost = (25 + (2 * self.turn)) ** 1.2
        cost = int(cost)        
        stdscr.clear()
        stdscr.addstr(1,1,"WORKFORCE:",curses.color_pair(heading) |curses.A_BOLD)
        tools.InsertLine(stdscr,2,"-",border)
        stdscr.addstr(3,1,"You may contract alien workers to speed up your expansion.",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(4,1,"A group of workers may be bought with credits.",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(5,1,"As your empire grows, the cost of workers will rise.",curses.color_pair(text_main) |curses.A_BOLD)
        tools.InsertLine(stdscr,6,"-",border)
        stdscr.addstr(7,1,"Current cost per group: "+str(cost),curses.color_pair(text_main) |curses.A_BOLD)
        tools.InsertLine(stdscr,8,"-",border)
        stdscr.addstr(19,1,"ENTER: Hire workers, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)
        self.BOTH_BG()

        ACTION = stdscr.getch()

        if ACTION == 13:
            if self.credits >= cost:
                self.credits -= cost
                self.action_points += 1
                self.LOG_ADD("Workers hired.",text_main)
            else:
                self.LOG_ADD("No credits available.",text_main)
            self.WORKFORCE()
                
        elif ACTION == 8:
            self.DISPLAY()        
        else:            
            self.WORKFORCE()
                
    def MARKET(self):
        """buy and sell resources"""
        self.COLONISED_UPDATE()
        self.UPDATE_PRICES()
        #Sets the maximum amount that can be bought/sold with one action point
        self.commerce_total = 0
        for i in PLANET_DICT:
            if PLANET_DICT[i].colonised == True:
                self.commerce_total += PLANET_DICT[i].INFRA_CURRENT["commerce"]
        total_planets = 0
        for i in self.COLONISED_DICT:
            total_planets += self.COLONISED_DICT[i]
        self.max_trade = (total_planets * 5) + self.commerce_total/5 + 5        
        amount = 0
        stdscr.clear()        
        stdscr.addstr(1,1,"MARKET:",curses.color_pair(heading) |curses.A_BOLD)
        tools.InsertLine(stdscr,2,"-",border)        
        stdscr.addstr(3,1,"RESOURCE:     CREDIT VALUE:     YOU HAVE:",curses.color_pair(text_main) |curses.A_BOLD)        
        stdscr.addstr(4,1,"(1) Food",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(4,15,str(self.food[4]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(4,33,str(self.food[0]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(5,1,"(2) Luxis",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(5,15,str(self.luxis[4]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(5,33,str(self.luxis[0]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(6,1,"(3) Steel",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(6,15,str(self.steel[4]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(6,33,str(self.steel[0]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(7,1,"(4) Titanium",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(7,15,str(self.titanium[4]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(7,33,str(self.titanium[0]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(8,1,"(5) Eptum",curses.color_pair(text_main) |curses.A_BOLD)        
        stdscr.addstr(8,15,str(self.eptum[4]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(8,33,str(self.eptum[0]),curses.color_pair(text_main) |curses.A_BOLD)        
        tools.InsertLine(stdscr,9,"-",border)
        stdscr.addstr(10,1,"Maximum amount per AP: "+str(self.max_trade),curses.color_pair(text_main) |curses.A_BOLD)
        tools.InsertLine(stdscr,11,"-",border)
        tools.ClearLine(stdscr,19)
        stdscr.addstr(19,1,"1-5: Select resource, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)               
        self.BOTH_BG()
        choice1 = {49:"food",50:"luxis",51:"steel",52:"titanium",53:"eptum"} #all mineral numbers
        choice2 = {98:"Buying",115:"Selling"} #(b)uy or (s)ell
        
        #Gets a mineral to buy or sell
        ACTION1 = stdscr.getch()
        if ACTION1 in choice1:
            tools.ClearLine(stdscr,13)
            tools.ClearLine(stdscr,19)
            stdscr.addstr(19,1,"b: Buy, s: Sell, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)
            stdscr.refresh()
            
            #Chooses buying or selling
            ACTION2 = stdscr.getch()
            if ACTION2 in choice2:
                tools.ClearLine(stdscr,10)
                stdscr.addstr(10,1,choice2[ACTION2]+" "+choice1[ACTION1]+". | Maximum amount per AP: "+str(self.max_trade),curses.color_pair(text_main) |curses.A_BOLD)
                tools.ClearLine(stdscr,12)
                tools.ClearLine(stdscr,19)
                stdscr.addstr(19,1,"1-4: Raise (+1/+10/+100/MAX), ENTER/SPACE - Confirm/Cancel, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)                
                stdscr.refresh()
                
                #Loop for choosing amount to buy or sell
                while 1:
                    ACTION3 = stdscr.getch()
                    temp = {49:1,50:10,51:100,52:self.max_trade}
                    if ACTION3 in temp:
                        amount += temp[ACTION3]
                    if amount > self.max_trade:
                        amount = self.max_trade
                    tools.ClearLine(stdscr,10)
                    stdscr.addstr(10,1,choice2[ACTION2]+" "+str(amount)+" "+choice1[ACTION1]+". | Maximum amount per AP: "+str(self.max_trade),curses.color_pair(text_main) |curses.A_BOLD)
                    stdscr.refresh()                    
                    if ACTION3 == 13 or ACTION3 == 32: #ENTER or SPACE
                        break

                tools.ClearLine(stdscr,12)
                for i in range(14,17):
                    tools.ClearLine(stdscr,i)
                
                #Handles the semantics of buying/selling
                if ACTION3 == 13:
                    if self.action_points >= 1:                        
                        tools.ClearLine(stdscr,13)
                        #NOTE: self.GAME_RESOURCES[temp[ACTION1]] refers to the resource you have selected
                        temp = {49:0,50:1,51:2,52:3,53:4}
                        
                        if ACTION2 == 98: #BUYING
                            #Checks to see if the player has enough credits for the transaction
                            if self.credits >= self.GAME_RESOURCES[temp[ACTION1]][4] * amount:                            
                                #Then the player gains the resource and loses credits
                                self.GAME_RESOURCES[temp[ACTION1]][0] += amount
                                #Take away credits: the value * the amount
                                self.credits -= self.GAME_RESOURCES[temp[ACTION1]][4] * amount
                                #Increase supply/demand value by amount bought
                                self.GAME_RESOURCES[temp[ACTION1]][5] += amount
                                if self.GAME_RESOURCES[temp[ACTION1]][5] > 5000:
                                    self.GAME_RESOURCES[temp[ACTION1]][5] = 5000                                                         
                                self.action_points -= 1
                                self.LOG_ADD("Transaction completed.",text_main)                                
                            else:
                                stdscr.addstr(14,1,"No credits available.",curses.color_pair(secondary) |curses.A_BOLD)
                                
                        if ACTION2 == 115: #SELLING
                            #Checks to see if the player has enough of a resource to sell it
                            if self.GAME_RESOURCES[temp[ACTION1]][0] >= amount:
                                #Then the player loses the resource and gains credits
                                self.GAME_RESOURCES[temp[ACTION1]][0] -= amount
                                #Give credits: the value * the amount
                                self.credits += self.GAME_RESOURCES[temp[ACTION1]][4] * amount
                                #Reduce supply/demand value by amount sold
                                self.GAME_RESOURCES[temp[ACTION1]][5] -= amount
                                if self.GAME_RESOURCES[temp[ACTION1]][5] < 0:
                                    self.GAME_RESOURCES[temp[ACTION1]][5] = 0 
                                self.action_points -= 1
                                self.LOG_ADD("Transaction completed.",text_main)                                 
                            else:
                                self.LOG_ADD("No "+choice1[ACTION1]+" available.",text_main)
                    else:
                        self.LOG_ADD("No action points available.",text_main)                        
                elif ACTION3 == 32:                    
                     self.LOG_ADD("Transaction cancelled.",text_main)           
        if ACTION1 == 8:
            self.DISPLAY()        
        else:            
            self.MARKET()

    def ARTEFACT_VIEW(self):
        """view player artefacts"""
        stdscr.clear()        
        stdscr.addstr(1,1,"ARTEFACTS:",curses.color_pair(heading) |curses.A_BOLD)
        tools.InsertLine(stdscr,2,"-",border)
        stdscr.addstr(19,1,"1-10: View artefact information, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)        
        stdscr.addstr(3,1," (1) Small Research Capsule: "+str(self.ARTEFACTS["Small Research Capsule"]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(4,1," (2) Medium Research Capsule: "+str(self.ARTEFACTS["Medium Research Capsule"]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(5,1," (3) Massive Research Capsule: "+str(self.ARTEFACTS["Massive Research Capsule"]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(6,1," (4) Miracle Seeds: "+str(self.ARTEFACTS["Miracle Seeds"]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(7,1," (5) Alchemist's Orb: "+str(self.ARTEFACTS["Alchemist's Orb"]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(8,1," (6) Metallic Shard: "+str(self.ARTEFACTS["Metallic Shard"]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(9,1," (7) Eptum Emitter: "+str(self.ARTEFACTS["Eptum Emitter"]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(10,1," (8) Trade Beacon: "+str(self.ARTEFACTS["Trade Beacon"]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(11,1," (9) Timewarp Device: "+str(self.ARTEFACTS["Timewarp Device"]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(12,1,"1(0) Ancient Temple: "+str(self.ARTEFACTS["Ancient Temple"]),curses.color_pair(text_main) |curses.A_BOLD)        
        tools.InsertLine(stdscr,13,"-",border)
        self.BOTH_BG()
        
        actions = {49:["Small Research Capsule",\
                       "A small-sized capsule containing some papers.",\
                       "Raises two random technologies by one research level each.",\
                       "No further benefits."],\
                   50:["Medium Research Capsule",\
                       "A medium-sized capsule containing a pile of research notes.",\
                       "Raises two random technologies by two research levels each.",\
                       "No further benefits."],\
                   51:["Massive Research Capsule",\
                       "An enormous capsule containing multiple research books.",\
                       "Raises all technologies by one research level each.",\
                       "No further benefits."],\
                   52:["Miracle Seeds",\
                       "Small golden seeds that possess a faint inner glow.",\
                       "No initial benefits.",\
                       "Increases food production for every colonised planet."],\
                   53:["Alchemist's Orb",\
                       "A large metallic orb with an ornate pattern carved into it's surface.",\
                       "No initial benefits.",\
                       "Generates small amounts of luxis."],\
                   54:["Metallic Shard",\
                       "A razer-sharp fragment of metal with a dull gleam to it.",\
                       "No initial benefits.",\
                       "Generates small amounts of steel and titanium."],\
                   55:["Eptum Emitter",\
                       "A dense black rock that absorbs almost all light striking it.",\
                       "No initial benefits.",\
                       "Generates small amounts of eptum."],\
                   56:["Trade Beacon",\
                       "A large flashing beacon easily seen from space.",\
                       "No initial benefits.",\
                       "Increases the maximum amount you may buy and sell from the market."],\
                   57:["Timewarp Device",\
                       "A large chamber with a sign on the front: Warning - Do Not Use!",\
                       "Generates a time distortion, giving you a large amount of action points.",\
                       "Generates a small amount of action points."],\
                   48:["Ancient Temple",\
                       "An ancient ruined temple, guarded by towering statues.",\
                       "Ancient priests slaughter the planet's population.",\
                       "Taxes on every colonised planet are greatly increased."]}
        
        ACTION = stdscr.getch()
        if ACTION in actions:            
            if self.ARTEFACTS[actions[ACTION][0]] >= 1: #Checks if player has 1 or more of the chosen artefact
                for i in range(3,13):
                    tools.ClearLine(stdscr,i)                
                stdscr.addstr(3,1,"Name:",curses.color_pair(heading) |curses.A_BOLD)
                stdscr.addstr(4,1,actions[ACTION][0],curses.color_pair(text_main) |curses.A_BOLD)
                stdscr.addstr(5,1,"Description:",curses.color_pair(heading) |curses.A_BOLD)
                stdscr.addstr(6,1,actions[ACTION][1],curses.color_pair(text_main) |curses.A_BOLD)
                stdscr.addstr(7,1,"Upon Discovery:",curses.color_pair(heading) |curses.A_BOLD)
                stdscr.addstr(8,1,actions[ACTION][2],curses.color_pair(text_main) |curses.A_BOLD)
                stdscr.addstr(9,1,"Every Turn:",curses.color_pair(heading) |curses.A_BOLD)
                stdscr.addstr(10,1,actions[ACTION][3],curses.color_pair(text_main) |curses.A_BOLD)
                tools.ClearLine(stdscr,13)
                tools.InsertLine(stdscr,11,"-",border)
                stdscr.addstr(19,1,"BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)                
                self.BOTH_BG()
                while 1:
                    RETURN = stdscr.getch()
                    if RETURN == 8:                        
                        break
                self.DISPLAY()
        if ACTION == 8:
            self.DISPLAY()
        else:            
            self.ARTEFACT_VIEW()

    def LOG_ADD(self,string,colour):        
        self.MSGLOG.append("#"+str(self.turn)+" "+string)
    
    def LOG_VIEW(self):
        """a full list of all things that have happened in the game"""        
        stdscr.clear()        
        stdscr.addstr(1,1,"MESSAGE LOG:",curses.color_pair(heading) |curses.A_BOLD)
        tools.InsertLine(stdscr,2,"-",border)              
        
        cur_dis = 0
        dis_lim = 15
        self.MSGLOG.reverse() 
        while 1:
            for y in range(5,19):
                stdscr.addstr(y,70,"|",curses.color_pair(border))
            stdscr.addstr(7,63,"Newer Messages",curses.color_pair(text_main) |curses.A_BOLD)        
            stdscr.addstr(15,63,"Older Messages",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(4,69,"/ \\",curses.color_pair(text_main) |curses.A_BOLD)
            stdscr.addstr(4,70,"'",curses.color_pair(border))
            stdscr.addstr(18,69,"\\ /",curses.color_pair(text_main) |curses.A_BOLD)        
            stdscr.addstr(18,70,"'",curses.color_pair(border))
            stdscr.addstr(19,1,"PAGEUP/PAGEDOWN: View newer/older messages, BACKSPACE: Return",curses.color_pair(secondary) |curses.A_BOLD)
        
            self.BOTH_BG()
            tools.Scroll(stdscr,self.MSGLOG,3,1,dis_lim,cur_dis,text_main)
            stdscr.refresh()
            for i in range(3,19):
                tools.ClearLine(stdscr,stdscr,i)
                
            INPUT = stdscr.getch()
            if INPUT == 339:
                cur_dis -= 1
                if cur_dis < 0:
                    cur_dis = 0
            elif INPUT == 338:
                cur_dis += 1
                if cur_dis > len(self.MSGLOG) - dis_lim:
                    cur_dis = len(self.MSGLOG) - dis_lim
            elif INPUT == 8:
                break
        
        self.MSGLOG.reverse() 
       
##        line_count = 3        
##        for i in self.MSGLOG:            
##            if line_count < 18:                
##                stdscr.addstr(line_count,1,i,curses.color_pair(text_main) |curses.A_BOLD)                
##                line_count += 1        
##        self.MSGLOG.reverse()        
##        self.BOTH_BG()

    def BRIBE(self):
        self.donated += self.credits
        self.credits = 0
        self.MAIN()

    def MAIN(self):
        """the default interface view: both backgrounds and GAME information/intro"""
        self.COLONISED_UPDATE()
        stdscr.clear()
        self.BOTH_BG()
        #Introduction and info        
        stdscr.addstr(1,1,"The human race is destroyed - a new empire is born.",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(2,1,"It's up to you to save humanity!",curses.color_pair(text_main) |curses.A_BOLD)
        tools.InsertLine(stdscr,3,"-",border)
        
        stdscr.addstr(4,1,"Primary Objective: "+self.objective,curses.color_pair(text_main) |curses.A_BOLD)
        
        if self.objective == "Colonise 10 planets in phase 10 space.":
            stdscr.addstr(5,1,"Progress: "+str(self.COLONISED_DICT["10"])+"/10 planets.",curses.color_pair(text_main) |curses.A_BOLD)
            
        elif self.objective == "Bribe your enemy with 5 million credits.":
            stdscr.addstr(5,1,"Progress: "+str(self.donated)+" credits so far - \"b\" to bribe all credits.",curses.color_pair(text_main) |curses.A_BOLD)
            
        elif self.objective == "Destroy all enemies in the solar system.":
            stdscr.addstr(5,1,"Progress: There are still enemies out there.",curses.color_pair(text_main) |curses.A_BOLD)
        
        tools.InsertLine(stdscr,6,"-",border)
        
        stdscr.addstr(7,1,"Empire Overview:",curses.color_pair(text_main) |curses.A_BOLD)        
        stdscr.addstr(8,1,"Food: %s" % self.food[0],curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(9,1,"Luxis: %s" % self.luxis[0],curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(10,1,"Steel: %s" % self.steel[0],curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(11,1,"Titanium: %s" % self.titanium[0],curses.color_pair(text_main) |curses.A_BOLD)        
        stdscr.addstr(12,1,"Eptum: %s" % self.eptum[0],curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(13,1,"Production Points: %s" % self.PP,curses.color_pair(text_main) |curses.A_BOLD)
        tools.InsertLine(stdscr,14,"-",border)
        
        stdscr.addstr(19,1,"ENTER: End turn, F1: Help, ESCAPE: Quit the game",curses.color_pair(secondary) |curses.A_BOLD)
        stdscr.refresh()

    def BOTH_BG(self):        
        """makes displaying both backgrounds together easier"""
        self.TOP_BG()
        self.BOTTOM_BG()

    def TOP_BG(self):
        """print the top part of the interface"""        
        #Top line
        for x in range(0,80):
            stdscr.addstr(0,x,border_char,curses.color_pair(border))
        #Side lines
        for y in range(0,21):
            stdscr.addstr(y,0,border_char,curses.color_pair(border))
            stdscr.addstr(y,79,border_char,curses.color_pair(border))
##        #Main fill    
##        for y in range(1,19):        
##            for x in range(0,80):            
##                stdscr.addstr(y,x,bg_char,curses.color_pair(background))
        stdscr.addstr(0,26," === A Planet's Revenge === ",curses.color_pair(heading) |curses.A_BOLD)        
        stdscr.refresh()
        
    def BOTTOM_BG(self):
        """prints the bottom bar of the interface"""
        tools.ClearLine(stdscr,21)
        tools.ClearLine(stdscr,22)
        #Fancy triangle edge (left)       
        stdscr.addstr(21,22,"\\",curses.color_pair(border))
        stdscr.addstr(22,22,"\\",curses.color_pair(border))        
        stdscr.addstr(22,23,"\\",curses.color_pair(border))
        #Fancy triangle edge (right)
        stdscr.addstr(21,32,"/",curses.color_pair(border))
        stdscr.addstr(22,31,"/",curses.color_pair(border))
        stdscr.addstr(22,32,"/",curses.color_pair(border))
        #Horizontals for bottom bar
        for x in range(0,22):
            stdscr.addstr(20,x,border_char,curses.color_pair(border))
        for x in range(33,80):
            stdscr.addstr(20,x,border_char,curses.color_pair(border))
        for x in range(0,80):
            stdscr.addstr(23,x,border_char,curses.color_pair(border))        
        #Verticals for bottom bar
        for y in range(21,23):            
            stdscr.addstr(y,0,border_char,curses.color_pair(border))
            stdscr.addstr(y,21,border_char,curses.color_pair(border))
            stdscr.addstr(y,33,border_char,curses.color_pair(border))
            stdscr.addstr(y,79,border_char,curses.color_pair(border))
        #Left-Bottom Player Info Bar
        stdscr.addstr(21,2,"Credits: %s" % self.credits,curses.color_pair(text_main) |curses.A_BOLD)        
        stdscr.addstr(22,2,"Turn: "+str(self.turn)+" | "+str(self.action_points)+" AP",curses.color_pair(text_main) |curses.A_BOLD)        
        #Middle-Bottom Name/Version Bar
        stdscr.addstr(20,23,"<company>",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(21,24,"v%s" % version,curses.color_pair(5) |curses.A_BOLD)
        stdscr.addstr(22,25,"~~*~~",curses.color_pair(1) |curses.A_BOLD)
        #Right-Bottom "Recent Messages" Bar        
        stdscr.addstr(21,35,self.MSGLOG[-1],curses.color_pair(text_main) |curses.A_BOLD)        
        stdscr.addstr(22,35,self.MSGLOG[-2],curses.color_pair(text_main))        
        stdscr.refresh()

    def EVENTS(self,context,planet = None):
        """works through all random events"""
        #Below are the default "nothing happening" messages
        self.turn_event = "Nothing of special interest has occured in our empire."
        self.planet_event = "Nothing of special interest has occured on our planets."
        self.col_event = "This planet appears fine to colonise.."
        self.exp_event = "Explorers have found nothing unusual."
        
        #General events that are triggered by ending the turn
        if context == "turn":                      
            seed = random.randint(1,100)
            if seed >= 1 and seed <= 3: #3% chance                
                self.turn_event = "A lucrative trade deal has paid off, gaining you credits."
                self.credits += random.randint(50,self.credits + 50)
            elif seed >= 4 and seed <= 6: #3% chance               
                self.turn_event = "A co-ordinated hacking attempt robs you of credits."
                self.credits -= random.randint(50,self.credits + 50)
                if self.credits <= 0:
                    self.credits = 0
            elif seed >= 7 and seed <= 16: #10% chance
                for i in TECH_DICT:
                    if self.current_tech == i:
                        TECH_DICT[i][2][TECH_DICT[i][0]] = 1
                        self.turn_event = "There has been a breakthrough in research - we have completed ahead of schedule!"                
            elif seed >= 17 and seed <= 21: #5% chance
                names = ["crashed","stabilised","boomed"]
                seed_names = random.randint(0,2)                
                self.turn_event = "The market has "+names[seed_names]+"!"
                states = ["down","steady","up"]
                for i in self.GAME_RESOURCES:
                    i[3] = states[seed_names]
                    
        #Planet-specific events that are triggered by ending the turn       
        elif context == "planet":
            seed = random.randint(1,100)
            if seed >= 1 and seed <= 3: #3% chance
                names = ["space-born virus","life-support failure","solar storm","eptum detonation","food shortage","gravity disruption"]
                seed_names = random.randint(0,len(names) - 1)
                self.planet_event = "A "+names[seed_names]+" on "+planet.name+" has resulted in high casaulties."
                planet.pop_current /= 2
            elif seed >= 4 and seed <= 5: #2% chance
                self.planet_event = "Meteor shower on "+planet.name+" - no survivors - meteoric metals mined."                
                self.steel[0] += random.randint(0,50 + planet.pop_current / 2) 
                self.titanium[0] += random.randint(0,20 + planet.pop_current / 3)
                planet.pop_current = 0
                
        #Planet-specific events that are triggered by finding a colonisable planet        
        elif context == "colonise":
            seed = random.randint(1,100)
            if seed >= 1 and seed <= 10: #10% chance
                self.col_event = "Native Population Status: - Friendly -"
                self.col_mod = -2
            elif seed >= 11 and seed <= 20: #10% chance
                self.col_event = "Native Population Status: - Polite -"
                self.col_mod = -1
            elif seed >= 21 and seed <= 30: #10% chance
                self.col_event = "Native Population Status: - Cautious -"
                self.col_mod = 1
            elif seed >= 31 and seed <= 40: #10% chance
                self.col_event = "Native Population Status: - Hostile -"
                self.col_mod = 2
            else: #60% chance
                self.col_event = "Native Population Status: - Neutral -"
                self.col_mod = 0
                
        #Planet-specific events that are triggered by exploring a planet's surface                
        elif context == "explore surface":
            temp = self.ARTEFACTS
            seed = random.randint(1,10000) #1000 = 10%, 100 = 1%, 10 = .1%, 1 = .01%
            
            #Research artefacts - raises tech levels                      
            
            if seed >= 0 and seed < 100:
                self.ARTEFACTS["Small Research Capsule"] += 1
                self.LOG_ADD("Small Research Capsule found: "+planet.name+"!",text_main)
                #Gives a random tech a +1 bonus to level (fuck checking for current tech and not raising that, maybe later)
                random_index = random.randint(0,9)                    
                TECH_DICT[random_index][0] += 1                
                        
            elif seed >= 100 and seed < 140:
                self.ARTEFACTS["Medium Research Capsule"] += 1
                self.LOG_ADD("Medium Research Capsule found: "+planet.name+"!",text_main)
                #Gives a random tech a +3 bonus to level (see above for more to do...)
                random_index = random.randint(0,9)                    
                TECH_DICT[random_index][0] += 3  
                        
            elif seed >= 140 and seed < 160:
                self.ARTEFACTS["Massive Research Capsule"] += 1
                self.LOG_ADD("Massive Research Capsule found: "+planet.name+"!",text_main)
                #Gives all techs a +1 bonus to level (except any being researched)
                for i in TECH_DICT:
                    if i != self.current_tech:
                        TECH_DICT[i][0] += 1
                    
            #Resource artefacts - gives resources on finding and per turn
            elif seed >= 160 and seed < 200:
                self.ARTEFACTS["Miracle Seeds"] += 1
                self.LOG_ADD("Miracle Seeds found: "+planet.name+"!",text_main)
                
            elif seed >= 200 and seed < 240:
                self.ARTEFACTS["Alchemist's Orb"] += 1
                self.LOG_ADD("Alchemist's Orb found: "+planet.name+"!",text_main)
                
            elif seed >= 240 and seed < 280:
                self.ARTEFACTS["Metallic Shard"] += 1
                self.LOG_ADD("Metallic Shard found: "+planet.name+"!",text_main)
                
            elif seed >= 280 and seed < 320:                
                self.ARTEFACTS["Eptum Emitter"] += 1
                self.LOG_ADD("Eptum Emitter found: "+planet.name+"!",text_main)
                
            #Special artefacts - gives miscellaneous bonuses
            elif seed >= 320 and seed < 340:
                self.ARTEFACTS["Trade Beacon"] += 1
                self.LOG_ADD("Trade Beacon found: "+planet.name+"!",text_main)
                
            elif seed >= 340 and seed < 350:
                self.ARTEFACTS["Timewarp Device"] += 1
                self.LOG_ADD("Timewarp Device found: "+planet.name+"!",text_main)
                
                self.action_points += 25
            elif seed >= 350 and seed < 351:
                self.ARTEFACTS["Ancient Temple"] += 1
                self.LOG_ADD("Ancient Temple found: "+planet.name+"!",text_main)
                planet.pop_current = 0

            #"Valuable Land" - giving a +1 bonus to all infra on the planet
            elif seed >= 351 and seed < 850: #5% chance
                self.LOG_ADD("Valuable land found on "+planet.name+"!",text_main)
                for i in planet.INFRA_CURRENT:
                    planet.INFRA_CURRENT[i] += 1
                    if planet.INFRA_CURRENT[i] > planet.INFRA_MAX[i]:
                        planet.INFRA_CURRENT[i] = planet.INFRA_MAX[i]
                
            #Set any techs that are > 10 back to max
            for i in TECH_DICT:
                if TECH_DICT[i][0] >= 10:
                    TECH_DICT[i][0] = 10

    def TURN(self):
               
        #Call secondary functions...        
        self.UPDATE_COMBAT() #Run through laser satellite vs. enemy ship battles
        self.UPDATE_PRICES() #Update resource prices on market
        self.UPDATE_SUPDEM() #Even out supply/demand values       
        self.EVENTS("turn") #Do events on turn
        self.UPDATE_ENEMY() #Spawn ships and upgrade enemy level
                
        #Increase turn count and action points
        self.turn += 1
        self.ap_gained = 2 #Standard amount
        points = {0:0,1:1,2:1,3:2,4:2,5:3,6:3,7:4,8:4,9:5,10:6}
        self.ap_gained += points[TECH_DICT["Space-Time Continuum"][0]] #Amount from research
        self.ap_gained += self.ARTEFACTS["Timewarp Device"] * 2 #Amount from artefacts
        self.action_points += self.ap_gained

        #Sets all to 0 for new end-of-turn
        self.food_add = 0
        self.food_minus = 0
        self.taxes = 1
        self.trade_taxes = 0
        self.lost_pop = 0
        self.pop_growth = 0
        self.PP = 0
        self.upkeep = 0
        self.mineral_gain = {"eptum":0,"luxis":0,"steel":0,"titanium":0}
        self.science_bonus = 0
        
        #Check for artefacts and add bonuses according to how many the player has
        self.mineral_gain["eptum"] += self.ARTEFACTS["Eptum Emitter"] * 25
        self.mineral_gain["luxis"] += self.ARTEFACTS["Alchemist's Orb"] * 25
        self.mineral_gain["steel"] += self.ARTEFACTS["Metallic Shard"] * 20
        self.mineral_gain["titanium"] += self.ARTEFACTS["Metallic Shard"] * 10        
         
        #Runs through all planets
        for i in PLANET_DICT:

            self.science_bonus += PLANET_DICT[i].INFRA_CURRENT["science"]
            
            #On all colonised planets...
            if PLANET_DICT[i].colonised == True:

                #Checks if planet is destroyed, sets destroy flag to true
                if PLANET_DICT[i].planet_health[0] <= 0:                    
                    PLANET_DICT[i].destroy = True
                    self.LOG_ADD(PLANET_DICT[i].name+" has been destroyed.",text_main)
                    
                #Business as usual!
                else:             
                            
                    #Update health according to research level
                    max_health = 100 + (PLANET_DICT[i].size * TECH_DICT["Planetary Shielding"][0]) * 100
                    
                    #"Heals" planet 10% of max health per turn
                    current_health = PLANET_DICT[i].planet_health[0] + PLANET_DICT[i].planet_health[1]/10
                    if current_health > max_health:
                        current_health = max_health
                        
                    #Places in health list [current,max]
                    PLANET_DICT[i].planet_health = [current_health,max_health]                    
                    
                    #Do food formulae                
                    self.food_add += PLANET_DICT[i].INFRA_CURRENT["ag"] * 2 + 3
                    self.food_minus += PLANET_DICT[i].pop_current / 3
                    
                    #Grow population
                    PLANET_DICT[i].pop_current += PLANET_DICT[i].pop_growth                
                    if PLANET_DICT[i].pop_current > PLANET_DICT[i].pop_max:                    
                        PLANET_DICT[i].pop_current = PLANET_DICT[i].pop_max
                        
                    #Update max population                
                    PLANET_DICT[i].pop_max = PLANET_DICT[i].type.type_pop_mod * (PLANET_DICT[i].size/2) * 5 * (PLANET_DICT[i].INFRA_CURRENT["lifesupport"] / 4 + 3)                                

                    #Update population growth                 
                    PLANET_DICT[i].pop_growth = PLANET_DICT[i].INFRA_CURRENT["lifesupport"] / 10 + 1                     

                    #Do tax formula
                    self.taxes += (PLANET_DICT[i].INFRA_CURRENT["commerce"]/2 + 10) * (PLANET_DICT[i].pop_current / 100)
                    self.taxes *= self.ARTEFACTS["Ancient Temple"] + 1

                    #Get taxes from trade satellites                    
                    for phase in SATELLITES:                       
                        self.trade_taxes += phase[2] * TECH_DICT["Satellite Technology"][0] # !!! DO FORMULA PROPERLY

                    #Updates cost of colonisation                
                    PLANET_DICT[i].cost = ((PLANET_DICT[i].type.type_cost_mod * PLANET_DICT[i].size +(random.randint(1,PLANET_DICT[i].type.type_cost_mod+(PLANET_DICT[i].size*2)))) * 50) - ((TECH_DICT["Rapid Expansion"][0] + 1) * 10)
                    if PLANET_DICT[i].cost <= 50:
                        PLANET_DICT[i].cost = 50

                    #Check for random events on the planet (FOR NOW: Only if there has been no events yet)
                    if self.planet_event == "Nothing of special interest has occured on our planets.":
                        self.EVENTS("planet",PLANET_DICT[i])                    

                    #Miracle seeds bonus
                    self.food_add += self.ARTEFACTS["Miracle Seeds"] * 10

                    #Adds to production points
                    self.PP += PLANET_DICT[i].INFRA_CURRENT["industry"] * random.randint(1,3)
                    
                    #Get minerals                
                    for j in PLANET_DICT[i].PLANET_MINERALS:
                        for k in self.GAME_RESOURCES:                            
                            if j == k[6]:
                                self.mineral_gain[j] += PLANET_DICT[i].INFRA_CURRENT["mine"] / 2 + 1   

                    #Update max infrastructure levels
                    upgrade_chance = TECH_DICT["Construction"][0]
                    for x in PLANET_DICT[i].INFRA_MAX:                
                        seed = random.randint(1,100)

                        #There is only a chance (from 1-10%) to upgrade all max infra on the planet by 1
                        if seed <= upgrade_chance:
                            PLANET_DICT[i].INFRA_MAX[x] += 1

                        #Sets a max infra level, equal to a base of 10 plus 5 times the planet size (15-60)
                        if PLANET_DICT[i].INFRA_MAX[x] > PLANET_DICT[i].size * 5 + 5:
                            PLANET_DICT[i].INFRA_MAX[x] = PLANET_DICT[i].size * 5 + 5                
                       
        #Decrease research turns left
        if self.current_tech != None:
            TECH_DICT[self.current_tech][2][TECH_DICT[self.current_tech][0]] -= 1
            #Check for research completion
            if TECH_DICT[self.current_tech][2][TECH_DICT[self.current_tech][0]] == 0:
                TECH_DICT[self.current_tech][0] += 1
                self.current_tech = None
        
        #Update research prices according to science infra levels
        self.science_bonus
        for i in TECH_DICT:
            for j in range(1,10):
                credit_cost = random.randint(10,30) + ((j**2) * 25) + random.randint(j,j*10) # - self.science_bonus
                TECH_DICT[i][1].append(credit_cost)

            for k in range(1,10):
                turn_cost = random.randint(4,6) + (k**2) - (3 * k) # - self.science_bonus
                TECH_DICT[i][2].append(turn_cost)

        #Update minerals gained
        for i in self.mineral_gain:
            for j in self.GAME_RESOURCES:
                if i == j[6]:
                    j[0] += self.mineral_gain[i]        
        
        if self.food_minus < 1:
            self.food_minus = 1
        
        #Update resources
        self.credits += self.taxes
        self.credits += self.trade_taxes
        self.food[0] += self.food_add
        self.food[0] -= self.food_minus
        
        #Checks for food below 0, which means some population will die
        if self.food[0] < 0:            
            self.lost_pop = abs(self.food[0]) / 10
            self.food[0] = 0
            if self.lost_pop < 1:                
                self.lost_pop = 1
                
        #This gets the first colonised planet found and kills lost_pop population, then breaks < wrong, now kills pop on all planets :P
        if self.lost_pop > 0:
            for i in PLANET_DICT:
                if PLANET_DICT[i].colonised == True:
                    PLANET_DICT[i].pop_current -= self.lost_pop        
 
        #Deletes planets that have been destroyed (better way to do this? see JD)
        total_count = 0
        done_count = 0
        #Gets the total amount of planets to be destroyed
        for i in PLANET_DICT:
            if PLANET_DICT[i].destroy == True:
                total_count += 1
        #While the amount done is less than the total, delete planets        
        while done_count < total_count:            
            for i in PLANET_DICT:
                if PLANET_DICT[i].destroy == True:
                    del PLANET_DICT[i]
                    done_count += 1
                    break        
              
        stdscr.clear()
        self.BOTH_BG()        
        stdscr.addstr(19,1,"ENTER: Cycle through turn information, BACKSPACE: Return to main screen",curses.color_pair(secondary) |curses.A_BOLD)
        stdscr.addstr(1,1,"TURN INFORMATION:",curses.color_pair(heading) |curses.A_BOLD)
        tools.InsertLine(stdscr,2,"-",border)        
        self.VIEW_GENERAL_TURN()
        stdscr.refresh()

        #After all the end of turn crap, has the player won or lost?
        self.WIN_LOSE()

        if self.status == "won":
            self.PLAYER_WIN()
            
        elif self.status == "lose":
            self.PLAYER_LOSE()
            
        elif self.status == None:
            viewing = 2 
            while 1:                        
                ACTION = stdscr.getch()
                stdscr.clear()
                self.BOTH_BG()
                stdscr.addstr(19,1,"ENTER: Cycle through turn information, BACKSPACE: Return to main screen",curses.color_pair(secondary) |curses.A_BOLD)            
                tools.InsertLine(stdscr,2,"-",border)
                
                if ACTION == 13:
                    if viewing == 1:                    
                        self.VIEW_GENERAL_TURN()
                        viewing = 2                
                    elif viewing == 2:                    
                        self.VIEW_ECONOMY_TURN()
                        viewing = 3                                  
                    elif viewing == 3:                   
                        self.VIEW_BATTLE_TURN()
                        viewing = 1
                    stdscr.refresh()
                    
                elif ACTION == 8:
                    break
                
            self.DISPLAY()

    def WIN_LOSE(self):
        #If the player has no planets but has had planets before...
        #self.status = "lost"
        
        if self.objective == "Colonise 10 planets in phase 10 space.":
            print self.COLONISED_DICT
            print self.COLONISED_DICT["10"]
            if self.COLONISED_DICT["10"] >= 10:
                self.status = "won"            
                
        elif self.objective == "Bribe your enemy with 5 million credits.":
            if self.donated >= 5000000:
                self.status = "won"
                
        elif self.objective == "Destroy all enemies in the solar system.":
           if self.total_ships_destroyed == 2:
                self.status = "won"

    def PLAYER_WIN(self):
        print "Win."
        stdscr.getch()

    def PLAYER_LOSS(self):
        print "Lose."
        stdscr.getch()

    def UPDATE_ENEMY(self):        
        #After a certain amount of turns, spawn enemy ships in phase 10
        turn_to_spawn_at = 20 - (self.difficulty * 2) + random.randint(-1,1)
        if self.turn >= turn_to_spawn_at:
            SHIPS[9] += random.randint(0,self.difficulty)       

        #There is a chance of the alien level increasing based on the difficulty level        
        seed = random.randint(1,100)        
        
        if (self.difficulty * 3) >= seed and self.turn >= turn_to_spawn_at:            
            self.alien_level += 1

        if self.alien_level >= 10:
            self.alien_level = 10            

    def UPDATE_COMBAT(self):

        ###########################################################################

        ship_acc = 50 + 5 * self.alien_level
        sat_acc = 50 + 5 * TECH_DICT["Satellite Technology"][0]

        #FOR EVERY PHASE, 10 > 1
        for i in range(9,-1,-1):

            #######################################################################
            
            #SHIPS > SATELLITES
            if SHIPS[i] > 0: #If there are enemy ships in a phase...                
                sat_count = 0
                sat_count += SATELLITES[i][0] + SATELLITES[i][1] + SATELLITES[i][2]
                if sat_count > 0: #Attack satellites here  
                    for ship in range(1,SHIPS[i] + 1):
                        seed = random.randint(1,100) #If satellite hits...                        
                        if ship_acc >= seed:
                            if SATELLITES[i][1] > 0: #Ship attacks shield satellites                    
                                SATELLITES[i][1] -= 1                                
                            elif SATELLITES[i][0] > 0: #Ship attacks laser satellites
                                SATELLITES[i][0] -= 1                               
                            elif SATELLITES[i][2] > 0: #Ship attacks trade satellites                            
                                SATELLITES[i][2] -= 1                                
                            
                else:  #Attack planets here                    
                    for ship in range(1,SHIPS[i] + 1):
                        for planet in PLANET_DICT:
                            if PLANET_DICT[planet].colonised == True and PLANET_DICT[planet].phase == i + 1:
                                planets_to_kill = True
                                break
                            else:
                                planets_to_kill = False
      
                        if planets_to_kill == True: #There is a planet to attack                                                        
                            seed = random.randint(1,100)
                            if ship_acc >= seed:
                                damage = 50 + ((self.alien_level ** 2) * random.randint(2,4))                                    
                                print PLANET_DICT[planet].name+" has suffered "+str(damage)                                    
                                PLANET_DICT[planet].planet_health[0] -= damage 
                             
                        elif planets_to_kill == False: #There are no planets here, move ships                      
                            if i > 0:                            
                                SHIPS[i - 1] += SHIPS[i] #Add number of ships to the next phase                            
                                SHIPS[i] = 0 #And set ships in original phase to 0                            

            ########################################################################

            #SATELLITES > SHIPS            
            if SATELLITES[i][0] > 0: #If player has laser satellites in a phase...                
                for satellite in range(1,SATELLITES[i][0] + 1): #For every laser satellite...                    
                    if SHIPS[i] > 0: #Only while there are enemy ships in the phase                        
                        #Satellites attack ship:
                        seed = random.randint(1,100)                        
                        if sat_acc >= seed:                            
                            SHIPS[i] -= 1
                            self.total_ships_destroyed += 1

            #########################################################################
                        
                
    def UPDATE_PRICES(self):
        """changes state and prices for each mineral available on the market"""        
        for i in self.GAME_RESOURCES:
            #The market price = min price + supdem value / (5000 / price range)            
            i[4] = i[1] + i[5]/(5000 / (i[2] - i[1]))            

    def UPDATE_SUPDEM(self):
        """updates poo!"""
        
        #For every mineral...
        for i in self.GAME_RESOURCES:
            
            #If supply/demand is above average, lower by 10%, but not below average
            if i[5] > 2500:
                i[5] -= i[5] / 10
                if i[5] < 2500:
                    i[5] = 2500
                    
            #If supply/demand is below average, raise by 10%, but not above average
            elif i[5] < 2500:
                i[5] += i[5] / 10
                if i[5] > 2500:
                    i[5] = 2500

    def VIEW_GENERAL_TURN(self):        
        stdscr.addstr(1,1,"TURN > GENERAL:   ",curses.color_pair(heading) |curses.A_BOLD)
        stdscr.addstr(3,1,"Turn number "+str(self.turn)+" has been completed.",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(4,1,"Action Points gained: "+str(self.ap_gained),curses.color_pair(text_main) |curses.A_BOLD)        
        tools.InsertLine(stdscr,5,"-",border)
        stdscr.addstr(6,1,"Empire Reports:",curses.color_pair(text_main) |curses.A_BOLD)               
        stdscr.addstr(7,1,self.planet_event,curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(8,1,self.turn_event,curses.color_pair(text_main) |curses.A_BOLD)        
        tools.InsertLine(stdscr,9,"-",border)
        stdscr.addstr(10,1,"Advice:",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(11,1,"\"You are advised to look for this feature some other time!\"",curses.color_pair(text_main) |curses.A_BOLD)
        tools.InsertLine(stdscr,12,"-",border)

    def VIEW_ECONOMY_TURN(self):        
        stdscr.addstr(1,1,"TURN > ECONOMY:",curses.color_pair(heading) |curses.A_BOLD)
        stdscr.addstr(3,1,"Food gained from agriculture: %s" % self.food_add,curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(4,1,"Food lost from hungry citizens: %s" % self.food_minus,curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(5,1,"Starved population: %s" % self.lost_pop,curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(6,1,"Taxes gained from law-abiding citizens: %s" % self.taxes,curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(7,1,"Taxes gained from trade satellites: %s" % self.trade_taxes,curses.color_pair(text_main) |curses.A_BOLD)
        tools.InsertLine(stdscr,8,"-",border)
        stdscr.addstr(9,1,"Mining Report:",curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(10,1,"Luxis gained: "+str(self.mineral_gain["luxis"]),curses.color_pair(text_main) |curses.A_BOLD)        
        stdscr.addstr(11,1,"Steel gained: "+str(self.mineral_gain["steel"]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(12,1,"Titanium gained: "+str(self.mineral_gain["titanium"]),curses.color_pair(text_main) |curses.A_BOLD)
        stdscr.addstr(13,1,"Eptum gained: "+str(self.mineral_gain["eptum"]),curses.color_pair(text_main) |curses.A_BOLD)
        tools.InsertLine(stdscr,14,"-",border)        

    def VIEW_BATTLE_TURN(self):
        stdscr.addstr(1,1,"TURN > BATTLE LOGS:",curses.color_pair(heading) |curses.A_BOLD)
        tools.InsertLine(stdscr,2,"-",border)
        stdscr.addstr(3,1,"mmk, put ships/satellites/planets destroyed for a start",curses.color_pair(text_main) |curses.A_BOLD)        
        
    def CHEAT(self,x):
        """adds x resources to your GAME, for testing only"""        
        for i in self.GAME_RESOURCES:
            i[0] += x
        self.LOG_ADD("H4cKz0rz!!",text_main)
        self.MAIN()

#Initialized first here so research for loop works at the first time
PLANET_DICT = {}


#########
# TECHS #
#########

# "Type" : [ [0] , [costs] , [turns] ]
TECH_DICT = {"Construction":[0,[],[]],
             "Rapid Expansion":[0,[],[]],
             "Planetary Shielding":[0,[],[]],
             "Planet Control":[0,[],[]],
             "Space-Time Continuum":[0,[],[]],
             "Satellite Technology":[0,[],[]],
             "Ship Manufacturing":[0,[],[]]}


for i in TECH_DICT:
    for j in range(1,10):
        credit_cost = random.randint(10,30) + ((j**2) * 25) + random.randint(j,j*10)
        TECH_DICT[i][1].append(credit_cost)

    for k in range(1,10):
        turn_cost = random.randint(4,6) + (k**2) - (3 * k)
        TECH_DICT[i][2].append(turn_cost)

debug = False

if debug == True:
    for i in TECH_DICT:
        print "---------------------------"
        print i
        print TECH_DICT[i][1]
        print TECH_DICT[i][2]
        print "---------------------------"

##############
# SATELLITES #
##############

# phase: [laser amount, shield amount, field amount]
SATELLITES = [[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0],[0,0,0]]

####################
# ENEMY SHIPS      #
####################

SHIPS = [0,0,0,0,0,0,0,0,0,0]

###########
# Planets #
###########

class PLANET:
    def __init__(self,name,phase):
        self.name = name
        self.phase = phase        
        self.set_stats()
                                    
    def set_stats(self):
        """sets stats for the planet"""
        if phase in range(1,10):
            self.colonised = True
        else:
            self.colonised = False
        self.destroy = False
        self.marker = ""
           
        type_choice = random.randint(1,6)
        if type_choice == 1:
            self.type = Terran
        elif type_choice == 2:
            self.type = Plains
        elif type_choice == 3:
            self.type = Oceanic
        elif type_choice == 4:
            self.type = Arctic
        elif type_choice == 5:
            self.type = Desert
        elif type_choice == 6:
            self.type = Volcanic
        
        self.type_name = self.type.type_name  #Type name comes from the planet's type
        self.size = random.randint(1,10)  #Size is a random number
        #Cost is the cost in credits to colonize a planet; based on planet size, type and is slightly random
        self.cost = ((self.type.type_cost_mod * self.size +(random.randint(1,self.type.type_cost_mod+(self.size*2)))) * 50) - ((TECH_DICT["Rapid Expansion"][0] + 1) * 10)
        if self.cost <= 50:
            self.cost = 50
        self.pop_current = 1  #How many citizens are on the planet; starts at 2-14 units of pop, each equals 1,000,000 people < wrong, now at 1
        self.pop_max = self.type.type_pop_mod * self.size * 10  #How many citizens can fit on the planet; based on lifesupport infra
        self.pop_growth = 0  #How many citizens the planet gains per turn; based on food supply and lifesupport infra

        health = 100 + (self.size * TECH_DICT["Planetary Shielding"][0]) * 100
        self.planet_health = [health,health] #Sets the min/max planet health

        self.explore_amount = [0,5 + random.randint(1,self.size) + (3 * self.size)]

        #All infra starts at level 1
        self.infra_lifesupport_current = 1  #Life Support raises population growth and max
        self.infra_mine_current = 1         #Mining raises speed of mining (more minerals/turn)
        self.infra_ag_current = 1           #Agricultural raises speed of farming (more food/turn)
        self.infra_science_current = 1      #Scientific raises speed of research (more research hours/AP)
        self.infra_commerce_current = 1     #Commerce raises amount of credit from taxes (credits/turn)
        self.infra_industry_current = 1     #Industrial raises the speed of ship construction
       
        #lifesupport, ag and mine are all based on a modifier from planet type, planet size and are slightly random        
        self.infra_lifesupport_max = self.type.type_pop_mod + random.randint(-2,1)     
        self.infra_mine_max = self.type.type_mine_mod + random.randint(-2,1)
        self.infra_ag_max = self.type.type_ag_mod + random.randint(-2,1)        
             
        #The last three are based on planet size and are more random than the first three              
        #This code basically keeps the levels fairly random and in the range of 1-10
        self.infra_science_max = self.size + random.randint(-1,1)
        self.infra_commerce_max = self.size + random.randint(-1,1)     
        self.infra_industry_max = self.size + random.randint(-1,1)        
        if self.infra_science_max > 8:
            self.infra_science_max = 8
            self.infra_science_max += random.randint(-1,2)                
        if self.infra_commerce_max > 8:
            self.infra_commerce_max = 8
            self.infra_commerce_max += random.randint(-1,2)
        if self.infra_industry_max > 8:
            self.infra_industry_max = 8
            self.infra_industry_max += random.randint(-1,2)
        #This checks to see if the three values are the same, if so it attempt to change them randomly
        if self.infra_science_max == (self.infra_commerce_max or self.infra_industry_max) or self.infra_commerce_max == self.infra_industry_max:
            self.infra_science_max += random.randint(-2,1)
            self.infra_commerce_max += random.randint(-2,1)
            self.infra_industry_max += random.randint(-2,1)
            
        #Checks to see if any infra_..._max has gone is <= 1, if so it set to 2
        if self.infra_lifesupport_max <= 1:
            self.infra_lifesupport_max = 2
        if self.infra_mine_max <= 1:
            self.infra_mine_max = 2
        if self.infra_ag_max <= 1:
            self.infra_ag_max = 2
        if self.infra_science_max <= 1:
            self.infra_science_max = 2
        if self.infra_commerce_max <= 1:
            self.infra_commerce_max = 2
        if self.infra_industry_max <= 1:
            self.infra_industry_max = 2

        #ONLY USE THESE DICTIONARIES TO ACCESS INFRA!
        self.INFRA_CURRENT = {"lifesupport":self.infra_lifesupport_current,"mine":self.infra_mine_current,"ag":self.infra_ag_current,"science":self.infra_science_current,"commerce":self.infra_commerce_current,"industry":self.infra_industry_current}
        self.INFRA_MAX = {"lifesupport":self.infra_lifesupport_max,"mine":self.infra_mine_max,"ag":self.infra_ag_max,"science":self.infra_science_max,"commerce":self.infra_commerce_max,"industry":self.infra_industry_max}
        
        #This code rolls the virtual dice to see what minerals the planet gets, and adds the name of them to a list    
        chances = [self.type.steel,self.type.titanium,self.type.luxis,self.type.eptum]
        self.PLANET_MINERALS = []
        for i in chances:
            seed = random.randint(1,100)
            if seed <= i[1]:
                self.PLANET_MINERALS.append(i[0])
        
##        #The mine_amount is the total amount of minerals available to be mined
##        #It's modified by the planet type, planet size as well as how many minerals are on the planet
##        self.mine_amount = self.type.type_mine_amount_mod * 25 * (self.size/2) * len(self.PLANET_MINERALS) + len(self.PLANET_MINERALS)
        
class PLANET_TYPE:
    def __init__(self,type_name,type_pop_mod,type_cost_mod,type_ag_mod,type_mine_mod,steel,titanium,luxis,eptum,type_desc):
        self.type_name = type_name
        self.type_pop_mod = type_pop_mod
        self.type_cost_mod = type_cost_mod
        self.type_ag_mod = type_ag_mod
        self.type_mine_mod = type_mine_mod               
        self.steel = ["steel",steel]
        self.titanium = ["titanium",titanium]
        self.luxis = ["luxis",luxis]    
        self.eptum = ["eptum",eptum]        
        self.type_desc = type_desc    

#Initializes all planet types with stats
Terran = PLANET_TYPE("Terran",9,2,8,8,25,25,40,20,"A standard Earth-like planet, with a good mix of terrain.")
Plains = PLANET_TYPE("Plains",10,1,9,5,25,25,90,45,"A rich land criss-crossed with rivers, perfect for agriculture.")
Oceanic = PLANET_TYPE("Oceanic",1,10,7,2,0,0,0,100,"Covered completely with water, only good for rare minerals.")
Arctic = PLANET_TYPE("Arctic",2,9,3,3,15,15,30,75,"Covered with hard ice shelfs and glaciers, only good for rare minerals.")
Desert = PLANET_TYPE("Desert",5,7,2,8,95,70,60,30,"A vast, sandy planet, good for mining.")
Volcanic = PLANET_TYPE("Volcanic",3,8,2,8,70,95,30,60,"An unstable, hot planet, good for mining.")

#PLANET_DICT is a major dictionary, containing all of the planet instances
PLANET_DICT = {}
#PHASE_COUNT is a major dictionary, containing the number of planet per phase
PHASE_COUNT = {}

#Creates the keys for phases and sets to 0
for i in range(1,11):
    PHASE_COUNT[str(i)] = 0

planet_amount = [random.randint(2,3),
                 random.randint(3,4),
                 random.randint(4,5),
                 random.randint(5,7),
                 random.randint(6,8),
                 random.randint(7,10),
                 random.randint(8,12),
                 random.randint(9,14),
                 random.randint(10,17),
                 random.randint(11,20)]

planets_temp = []

for line in open(".\planets.ini"):    
        planetname = line[:-1]
        planets_temp.append(planetname)

#Shuffle the planets around so it's not the same every time
random.shuffle(planets_temp)

count = 0

try:
    for i in range(0,10):
        for j in range(1,planet_amount[i]):
            phase = i + 1
            PHASE_COUNT[str(phase)] += 1    
            PLANET_DICT[planets_temp[count]] = PLANET(planets_temp[count],phase)
            count += 1

except:
    print "Error: Planet init failed (planets.ini missing?)"
    time.sleep(3)

###################
#GAME STARTS HERE #
###################

#Initializes main "GAME" class
GAME = GAME()

#Loads interface settings from a config file, if fails it loads defaults
try:
    for line in file(".\interface.ini", "r+"):
        INTERFACE = line[:-1].split("\t")
except:
    print "Error: interface.ini missing, loading default values."
    INTERFACE = [3,1,7,4,7," ","#","|"]

border = int(INTERFACE[0])
heading = int(INTERFACE[1])
text_main = int(INTERFACE[2])
secondary = int(INTERFACE[3])
background = int(INTERFACE[4])
bg_char = INTERFACE[5]
border_char = INTERFACE[6]
divide_char = INTERFACE[7]

#Adds the two intro messages to the message log
GAME.LOG_ADD("Welcome to A Planet's Revenge!",text_main)
GAME.LOG_ADD("Look for recent game messages here.",text_main)

#Starts curses with wrapper
curses.wrapper(main)
