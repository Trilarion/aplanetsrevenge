--- History --- 

Chapter 1: "Before the Invasion"

The population of Earth had achieved a lot since the turn of the 21st century. There was acknowledgment of global warming, a unification of the world's major powers, the establishment of a small self-sufficient colony on the Moon and above all, the start of a research program concerning faster-than-light (FTL) engines and self-sufficient space stations. The moon colonies led to plans of gigantic space-stations that could support life, which was a lot more than could be said about FTL travel, which was a mere dream.

A lot of terrible things had also happened. Mass extinctions of animal and plant species occured frequently until the only thing left were a few feral species and of course, bamboo. Global warming had caused numerous coastal cities to be swallowed by the rising tides, and world oil supplies had dwindled away, leaving the Earth reliant on semi-effective alternative energy sources.

The discovery of a super-heavy element deep below the Earth's surface, dubbed Eptum, changed everything. Scientists discovered ways to release collosal amounts of energy by reacting this valued new substance. This kick-started the Earth's manufacturing industries and economy, which had been lagging since the "peak oil" theory had become realised. Further research led to even more valuable uses - FTL travel. The energy released could be harnessed, propelling ships around the solar system. These tiny, unmanned ships consumed Eptum at an extreme rate, while only managing to travel a fraction over light speed. Primitive shielding methods were also developed using Eptum as a kind of energy absorption device.

A decade of intensive research, and things that were dreams years before were now reality. Early research goals had been combined and realised - gigantic platforms capable of FTL travel and supporting millions of people indefinately were know being designed and built. Shielding technology had grown advanced enough to be of practical use, but since the world was at peace, this was fairly useless. Even so, advancing knowledge of physics allowed new weaponry to be build, with powerful photon weaponry being just one type available.

The first fully-operational "Planetary Station" was built in the year 2048, one year before total disaster...


Chapter 2: "The Invasion"

In the year 2049, Earth was invaded by race of advanced starfaring people. They were much like humans, but were further along the path of FTL travel, a technology that allowed rapid expansion of a race.

They had been observing the planet for many years. They were with us when the first moon colony was created, when the great Peace Declaration of 2016 was put developed and when we were slowly poisoning our planet and everything living in it.

When they saw research escalating, leading to technology similar to their own, they realised they needed to act.

Three-thousand highly advanced planet-bombers descended into the atmosphere, unleased millions of E-bombs and escaped, leaving 12 billion citizens of Earth to burn in a fiery holocaust.

However, all was not lost. On the other side of the Earth, the SS Andromeda and it's 1 million passengers observed a false dawn envelope the Earth. This was the first and now the only Planetary Station. It had been on it's maiden orbit for almost a year, and was scheduled to return to Earth in one month. Now that would be impossible. The ship's captain, an intelligent leader with enough knowledge to know what to do next, set the ship on a course out of the Earth's orbit. With the next generation FTL engines taking them on a path out past the sun's influence, they realised the need to quickly find a habitable planet to restock their ship.

They were on the run from an advanced alien race, and their Eptum supplies were dwindling. The next few months would determine the course of the only survivors of the once strong human race...


Chapter Three: "Resources of a New Age"

New resources were always a catalyst to human expansion. After the new millenium, the Earth was far from mined out. 

Carbonite was the first new element to be discovered. It was relatively light substance and first appeared to be nothing unusual. It's unusual properties when it was placed into contact with a gold nugget. The substance seemed to vaporise and the nugget grew to 150% of it's original size. Further research showed that it could rapidly purify many ores into metallic substances, as well as expanding many other elements.

Stellic and Titanik where two types of ores found in certain environments on Earth. They were unlike other ores - they didn't have a base mineral, they simply grew on the rock as if organic. They had one special property that made them particularly valuable; they reacted extremely well to Carbonite, producing strong metallic alloys - named Steel and Titanium to replace the old, outdated metals.

Physical coins and notes were on their way out as the currency of choice - the new standard was credits, which were stored as data, and could be transported on small chips or transfered at the click of a button. 

After a series of gold mines stretching over 20 kilometres deep was found in the Himilayan mountains, the value of this once precious metal dropped enormously. It became a common object for most to wear. A new element, found first on the moon and then in remote locations on Earth was the new gold. It was dubbed luxis, after the lunar explorer who first discovered it. It had a pale green-gold colouring and had a beautiful, hypnotic quality. The inhabitants of the SS Andromeda soon discovered it prized throughout the universe, being one of the few things of value that could not be faked. Some civilisations almost went as far as to worship the substance.

These new resources helped the Earth develop, but none so much as Eptum. It was at least five times heavier than uranium, and much more unstable. Current uses including FTL travel, powerful shields and the infamous E-Bomb that destroyed the Earth. The first two are quite practical, eventually being advance enough to go through small amounts of Eptum for a lot of energy released. The second however is much different. It expends a huge amount of Eptum in order to completely vaporise up to three kilometres of hard crust from a planet. The radiation it leaves behind is proportional to the amount of Eptum expended, which is why the Earth was left as a shell of what it had once been, put back millions of years into a long ice age.


--- Interface Configuration ---

To change the interface, you must edit the values found in the file "config.ini", located in the main game folder.
The file must have 8 values, each separated by a tab. The last value must have a tab after it. 

Note to non-Windows Vista users:
The default values may appear to be small boxes. This does not affect the reading of values, so these can still be deleted and changed to whatever you want.

The values are (in order):
Border Colour - the main game border, always a dull colour
Heading Colour - main title and titles for areas, always bold
Main Text - standard text, normally white, always bold
Secondary Text - rarely used, for important things, always bold
Background Colour - colour of the "fill", always dull
Background Symbol - background pattern, always dull
Border Symbol - main border pattern, always dull
Divider Symbol - divider for the bottom bar, always dull

Current default values are 3 2 7 6 7 (space)  # |

The last three values can take any single character.

The first five values can be changed into any of these colours:
1 - red
2 - green
3 - blue
4 - yellow
5 - cyan
6 - magenta
7 - white
8 - solid white
9 - solid black
10 - solid blue
